package com.guimata.controller.normalForms;

import com.libfsm.automata.machines.DFA;
import com.libfsm.automata.machines.FSM;
import com.libfsm.automata.machines.NFA;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.FileChooser;

import java.io.File;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class NFHomeController implements Initializable {

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        NFControllerMediator.getInstance().setNfHomeController(this);
    }

    @FXML
    void changeToDFAView(ActionEvent event) {
        FSM machine = new DFA("empty");
        NFControllerMediator.getInstance().loadNFBuildFSM(machine);
    }

    @FXML
    void changeToNFAView(ActionEvent event) {
        FSM machine = new NFA("empty");
        NFControllerMediator.getInstance().loadNFBuildFSM(machine);
    }

    @FXML
    void importFSM(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("JSON", "*.json"));
        Optional<File> selectedFile = Optional.ofNullable(fileChooser.showOpenDialog(null));

        if (selectedFile.isPresent() && selectedFile.get().getName().endsWith(".json")) {
            FSM machine = FSM.readFromFile(selectedFile.get());
            NFControllerMediator.getInstance().loadNFBuildFSM(machine);
        }
    }

    @FXML
    void changeToExamplesView(ActionEvent event) {
        NFControllerMediator.getInstance().loadNFExamples();
    }
}
