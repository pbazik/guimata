package com.guimata.controller.normalForms;

import com.guimata.controller.computation.CControllerMediator;
import com.libfsm.automata.machines.FSM;
import com.libfsm.examples.dfa.DFAEx01;
import com.libfsm.examples.nfa.NFAEx01;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import java.net.URL;
import java.util.ResourceBundle;

public class NFExamplesController implements Initializable {

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        NFControllerMediator.getInstance().setNfExamplesController(this);
    }

    @FXML
    void changeToHome(ActionEvent event) {
        NFControllerMediator.getInstance().loadNFHome();
    }

    @FXML
    void endsWithAButton(ActionEvent event) {
        FSM machine = NFAEx01.get();
        NFControllerMediator.getInstance().loadNFBuildFSM(machine);
    }

    @FXML
    void numberAsButton(ActionEvent event) {
        FSM machine = DFAEx01.get();
        NFControllerMediator.getInstance().loadNFBuildFSM(machine);
    }
}
