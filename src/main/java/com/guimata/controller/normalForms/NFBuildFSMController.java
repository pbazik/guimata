package com.guimata.controller.normalForms;

import com.github.jabbalaci.graphviz.GraphViz;
import com.guimata.controller.common.BuildFSM;
import com.guimata.controller.computation.CControllerMediator;
import com.guimata.controller.operations.OpControllerMediator;
import com.jfoenix.controls.*;
import com.libfsm.automata.helpers.Sets;
import com.libfsm.automata.machines.DFA;
import com.libfsm.automata.machines.FSM;
import com.libfsm.automata.machines.FSMType;

import com.libfsm.automata.machines.NFA;
import com.libfsm.normalforms.common.NormalFormType;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.stream.Collectors;

import static com.guimata.controller.common.BuildFSM.displayInfoDialog;

public class NFBuildFSMController implements Initializable {
    GraphViz graphViz = new GraphViz();
    FSM machine;

    @FXML
    private ScrollPane scrollPane;

    @FXML
    private JFXRadioButton stepRadioButton;

    @FXML
    private ToggleGroup toggleGroup;

    @FXML
    private JFXRadioButton directRadioButton;

    @FXML
    private Text alphabetText;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        NFControllerMediator.getInstance().setNfBuildFSMController(this);
    }

    @FXML
    void changeToComputationHome(ActionEvent event) {
        NFControllerMediator.getInstance().loadNFHome();
    }

    @FXML
    void addState(ActionEvent event) {
        BuildFSM.addState(NFControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void removeState(ActionEvent event) {
        BuildFSM.removeState(NFControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void renameState(ActionEvent event) {
        BuildFSM.renameState(NFControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void addTransition(ActionEvent event) {
        BuildFSM.addTransition(NFControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void removeTransition(ActionEvent event) {
        BuildFSM.removeTransition(NFControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void defineAlphabet(ActionEvent event) {
        BuildFSM.defineAlphabet(NFControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void clearAutomaton(ActionEvent event) {
        if (machine.getFsmType().equals(FSMType.DFA))
            machine = new DFA("empty");
        else
            machine = new NFA("empty");
        BuildFSM.displayDiagramAndAlphabet(scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void setInitial(ActionEvent event) {
        BuildFSM.setInitial(NFControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void chooseNormalForm(ActionEvent event) {
        if (!machine.isCorrectlyDefined()) {
            BuildFSM.displayInfoDialog(NFControllerMediator.getInstance().getRoot(), machine.notCorrectlyDefinedText());
            return;
        }

        JFXDialogLayout content = new JFXDialogLayout();
        VBox inputItems = new VBox(10);

        Set<String> normalForms = new HashSet<>();

        if (machine.getFsmType().equals(FSMType.NFA))
            normalForms.addAll(Set.of("epsilon-free", "piggy"));
        normalForms.add("reachable");

        ToggleGroup tg = new ToggleGroup();

        for (String normalForm : normalForms) {
            JFXRadioButton formButton = new JFXRadioButton();
            formButton.setText(normalForm);
            tg.getToggles().add(formButton);
            inputItems.getChildren().add(formButton);
        }

        content.setBody(inputItems);

        JFXButton cancel = new JFXButton("Cancel");
        cancel.setButtonType(JFXButton.ButtonType.RAISED);
        JFXButton run = new JFXButton("Run");
        run.setButtonType(JFXButton.ButtonType.RAISED);
        content.setActions(cancel, run);

        JFXDialog dialog = new JFXDialog(NFControllerMediator.getInstance().getRoot(), content, JFXDialog.DialogTransition.CENTER);
        content.setPrefWidth(200);
        dialog.setPrefWidth(200);
        cancel.setOnAction(e -> dialog.close());
        run.setOnAction(e -> {
            if (tg.getSelectedToggle() == null) {
                BuildFSM.displayInfoDialog(NFControllerMediator.getInstance().getRoot(), "Select which normal form to construct!");
                dialog.close();
                return;
            }

            if (!machine.isCorrectlyDefined()) {
                displayInfoDialog(CControllerMediator.getInstance().getRoot(), machine.notCorrectlyDefinedText());
                dialog.close();
                return;
            }

            for (var state : machine.getStates()) {
                if (state.startsWith("<init")) {
                    BuildFSM.displayInfoDialog(NFControllerMediator.getInstance().getRoot(), "Names of states can't contain prefix \"<init\" !");
                    dialog.close();
                    return;
                } else if (state.startsWith("<final")) {
                    BuildFSM.displayInfoDialog(NFControllerMediator.getInstance().getRoot(), "Names of states can't contain prefix \"<final\" !");
                    dialog.close();
                    return;
                }
            }

            boolean directConstruction = directRadioButton.isSelected();
            JFXRadioButton selected = (JFXRadioButton)tg.getSelectedToggle();

            switch (selected.getText()) {
                case "reachable":
                    NFControllerMediator.getInstance().setType(NormalFormType.Reachable);
                    break;
                case "epsilon-free":
                    NFControllerMediator.getInstance().setType(NormalFormType.EpsilonFree);
                    break;
                case "piggy":
                    NFControllerMediator.getInstance().setType(NormalFormType.Piggy);
                    break;
            }

            NFControllerMediator.getInstance().loadNFRunFSM(machine, directConstruction);
            dialog.close();
        });
        dialog.show();
    }

    @FXML
    void exportToFile(ActionEvent event) {
        BuildFSM.exportToFile(machine);
    }

    @FXML
    void renameFSM(ActionEvent event) {
        BuildFSM.renameFSM(NFControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    public void loadFSM(FSM machine) {
        this.machine = machine;
        BuildFSM.displayDiagramAndAlphabet(scrollPane, graphViz, machine, alphabetText);
        alphabetText.setText(String.join(",", machine.getAlphabet()
                .stream()
                .map(Object::toString)
                .collect(Collectors.toSet())));

    }
}
