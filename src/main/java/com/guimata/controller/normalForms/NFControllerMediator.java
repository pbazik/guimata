package com.guimata.controller.normalForms;

import com.guimata.controller.common.MainController;
import com.libfsm.automata.machines.FSM;
import com.libfsm.normalforms.common.NormalFormType;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;

import java.io.IOException;


public class NFControllerMediator {
    private MainController mainController;
    private NFHomeController nfHomeController;
    private NFBuildFSMController nfBuildFSMController;
    private NFRunFSMController nfRunFSMController;
    private NFExamplesController nfExamplesController;

    private NormalFormType type;

    @FXML
    private StackPane root;

    public void setMainController(MainController mainController) {
        this.mainController = mainController;
        root = mainController.getNfStackPane();
    }

    public void setNfHomeController(NFHomeController nfHomeController) {
        this.nfHomeController = nfHomeController;
    }

    public void setNfBuildFSMController(NFBuildFSMController nfBuildFSMController) {
        this.nfBuildFSMController = nfBuildFSMController;
    }

    public void setNfRunFSMController(NFRunFSMController nfRunFSMController) {
        this.nfRunFSMController = nfRunFSMController;
    }

    public void setNfExamplesController(NFExamplesController nfExamplesController) {
        this.nfExamplesController = nfExamplesController;
    }

    public void setType(NormalFormType type) {
        this.type = type;
    }

    public NormalFormType getType() {
        return type;
    }

    public StackPane getRoot() {
        return root;
    }

    public void loadNFHome() {
        try {
            AnchorPane NFHome = FXMLLoader.load(getClass().getResource("/view/normalForms/NFHome.fxml"));
            root.getChildren().setAll(NFHome);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadNFExamples() {
        try {
            AnchorPane NFHome = FXMLLoader.load(getClass().getResource("/view/normalForms/NFExamples.fxml"));
            root.getChildren().setAll(NFHome);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadNFBuildFSM(FSM machine) {
        try {
            AnchorPane NFBuildFSM = FXMLLoader.load(getClass().getResource("/view/normalForms/NFBuildFSM.fxml"));
            root.getChildren().setAll(NFBuildFSM);
            nfBuildFSMController.loadFSM(machine);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadNFRunFSM(FSM machine, boolean direct) {
        try {
            AnchorPane NFRunFSM = FXMLLoader.load(getClass().getResource("/view/normalForms/NFRunFSM.fxml"));
            root.getChildren().setAll(NFRunFSM);
            nfRunFSMController.runNFFSMConstruction(machine, direct);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private NFControllerMediator() {}

    public static NFControllerMediator getInstance() {
        return ControllerMediatorHolder.INSTANCE;
    }

    private static class ControllerMediatorHolder {
        private static final NFControllerMediator INSTANCE = new NFControllerMediator();
    }
}
