package com.guimata.controller.normalForms;

import com.github.jabbalaci.graphviz.GraphViz;
import com.guimata.controller.common.BuildFSM;
import com.guimata.controller.common.RunFSM;
import com.guimata.controller.operations.OpControllerMediator;
import com.jfoenix.controls.JFXButton;
import com.libfsm.automata.helpers.Pair;
import com.libfsm.automata.helpers.Sets;
import com.libfsm.automata.helpers.Triple;
import com.libfsm.automata.machines.DFA;
import com.libfsm.automata.machines.FSM;
import com.libfsm.automata.machines.NFA;
import com.libfsm.normalforms.common.NormalFormType;
import com.libfsm.normalforms.common.NormalFormFactory;
import com.libfsm.operations.unary.UnaryOperation;
import com.libfsm.operations.unary.UnaryOperationFactory;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.*;

public class NFRunFSMController implements Initializable {
    private Map<Integer, String> steps = new HashMap<>();
    private UnaryOperation normalForm;
    private GraphViz graphViz = new GraphViz();
    private FSM machine;
    private FSM resultMachine;
    private FSM draw;
    private boolean direct = false;
    private int algorithmLength;
    private int step;
    private Iterator iterator;

    @FXML
    private VBox leftVbox;

    @FXML
    private ScrollPane leftScrollPane;

    @FXML
    private Text leftAlphabet;

    @FXML
    private VBox rightVbox;

    @FXML
    private ScrollPane rightScrollPane;

    @FXML
    private Text rightAlphabet;

    @FXML
    private JFXButton nextButton;

    @FXML
    private ScrollPane descriptionScrollPane;

    @FXML
    private Text descriptionText;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        NFControllerMediator.getInstance().setNfRunFSMController(this);
        HBox.setHgrow(leftVbox, Priority.ALWAYS);
        HBox.setHgrow(rightVbox, Priority.ALWAYS);
        VBox.setVgrow(leftScrollPane, Priority.ALWAYS);
        VBox.setVgrow(rightScrollPane, Priority.ALWAYS);
        descriptionScrollPane.setVvalue(1.0);
    }

    @FXML
    void changeToFSMBuild(ActionEvent event) {
        NFControllerMediator.getInstance().loadNFBuildFSM(machine);
    }

    @FXML
    void nextStep(ActionEvent event) {
        if (direct) {
            machine = resultMachine;
            BuildFSM.displayInfoDialog(NFControllerMediator.getInstance().getRoot(), "Completed");
            BuildFSM.displayDiagramAndAlphabet(rightScrollPane, graphViz, resultMachine, rightAlphabet);
            nextButton.setDisable(true);
            return;
        }

        if (step >= algorithmLength) {
            machine = resultMachine;
            BuildFSM.displayInfoDialog(NFControllerMediator.getInstance().getRoot(), "Completed");
            BuildFSM.displayDiagramAndAlphabet(rightScrollPane, graphViz, draw, rightAlphabet);
            nextButton.setDisable(true);
            return;
        }

        if (!iterator.hasNext()) {
            step++;

            switch (NFControllerMediator.getInstance().getType()) {
                case EpsilonFree:
                    switch (step) {
                        case 1:
                            iterator = machine.getAlphabet().iterator();
                            break;

                        case 2:
                            iterator = machine.getStates().iterator();
                            break;

                        case 3:
                            var transitionsNew = new HashSet<Pair<Triple<String, Character, String>, Triple<String, Character, String>>>();
                            machine.getStates().forEach(q -> {
                                var eClosure = machine.epsilonClosure(Set.of(q));
                                eClosure.forEach(eq -> {
                                    if (!eq.equals(q)) {
                                        machine.getAllTransitionsFrom(eq).forEach(pair -> {
                                            transitionsNew.add(new Pair<>(new Triple<>(eq, pair.getFirst(), pair.getSecond()), new Triple<>(q, pair.getFirst(), pair.getSecond())));
                                        });
                                    }
                                });
                            });
                            iterator = transitionsNew.iterator();
                            break;

                        case 4:
                            var transitionsOld = new HashSet<Triple<String, Character, String>>();
                            machine.getTransitions().forEach((k, v) -> {
                                v.forEach(q -> {
                                    if (!k.getSecond().equals(FSM.EPSILON))
                                        transitionsOld.add(new Triple<>(k.getFirst(), k.getSecond(), q));
                                });
                            });

                            iterator = transitionsOld.iterator();
                            break;

                        case 5:
                            iterator = Set.of(machine.getInitial()).iterator();
                            break;

                        case 6:
                            iterator = machine.getFinalStates().iterator();
                            break;

                        case 7:
                            var initFinal = new HashSet<String>();
                            machine.getStates().forEach(q -> {
                                if (machine.epsilonClosure(Set.of(q)).stream().anyMatch(machine::isFinalState))
                                    initFinal.add(q);
                            });
                            iterator = initFinal.iterator();
                            break;
                    }
                    break;


                case Piggy:
                    switch (step) {
                        case 1:
                            iterator = machine.getAlphabet().iterator();
                            break;

                        case 2:
                            iterator = machine.getStates().iterator();
                            break;

                        case 3:
                            var newStates = Set.of(resultMachine.getInitial(), resultMachine.getFinalStates().iterator().next());
                            iterator = newStates.iterator();
                            break;

                        case 4:
                            var transitionsOld = new HashSet<Triple<String, Character, String>>();
                            machine.getStates().forEach(source ->
                                    machine.getStates().forEach(destination ->
                                            machine.getAllTransitionsFromTo(source, destination).forEach(c ->
                                                    transitionsOld.add(new Triple<>(source, c, destination))
                                            )
                                    )
                            );
                            iterator = transitionsOld.iterator();
                            break;

                        case 5:
                            var transitionsNew = new HashSet<Triple<String, Character, String>>();

                            transitionsNew.add(new Triple<>(resultMachine.getInitial(), FSM.EPSILON, machine.getInitial()));
                            machine.getFinalStates().forEach(state ->
                                    transitionsNew.add(new Triple<>(state, FSM.EPSILON, resultMachine.getFinalStates().iterator().next()))
                            );
                            iterator = transitionsNew.iterator();
                            break;

                        case 6:
                            var initialState = Set.of(resultMachine.getInitial());
                            iterator = initialState.iterator();
                            break;

                        case 7:
                            var finalState = Set.of(resultMachine.getFinalStates().iterator().next());
                            iterator = finalState.iterator();
                            break;
                    }
                    break;


                case Reachable:
                    switch (step) {
                        case 1:
                            Set<String> reachableStates = new HashSet<>();
                            reachableStates.add(machine.getInitial());

                            Set<String> newStates = new HashSet<>();
                            newStates.add(machine.getInitial());

                            do {
                                Set<String> temp = new HashSet<>();
                                for (String state : newStates) {
                                    for (Character c : machine.getAlphabet()) {
                                        temp = Sets.union(temp, machine.computeStep(Set.of(state), c));
                                    }
                                }
                                newStates = Sets.difference(temp, reachableStates);
                                reachableStates = Sets.union(reachableStates, newStates);
                            } while (!newStates.isEmpty());

                            var unreachableStates = new HashSet<>(Sets.difference(machine.getStates(), reachableStates));
                            iterator = unreachableStates.iterator();
                            break;

                        case 2:
                            iterator = draw.getStates().iterator();
                            draw = machine.copy();
                            break;
                    }
                    break;
            }

            BuildFSM.displayDiagramAndAlphabet(leftScrollPane, graphViz, machine, leftAlphabet);
            BuildFSM.displayDiagramAndAlphabet(rightScrollPane, graphViz, draw, rightAlphabet);

            descriptionText.setText(descriptionText.getText().concat(steps.get(step)).concat("\n"));
            descriptionScrollPane.setVvalue(1.0);
            return;
        }

        switch (NFControllerMediator.getInstance().getType()) {
            case EpsilonFree:
                switch (step) {
                    case 0:
                        iterator.next();
                        break;

                    case 1:
                        var symbol = (Character)iterator.next();
                        draw.setAlphabet(Sets.union(draw.getAlphabet(), Set.of(symbol)));
                        BuildFSM.displayDiagramAndAlphabet(rightScrollPane, graphViz, draw, rightAlphabet);
                        descriptionText.setText(descriptionText.getText()
                                .concat("Added symbol \'")
                                .concat(symbol.toString())
                                .concat("\' to alphabet")
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);
                        break;

                    case 2:
                        var state = (String)iterator.next();
                        draw.addState(state);

                        RunFSM.displayStatesColored(leftScrollPane, graphViz, machine, Set.of(state), leftAlphabet);
                        RunFSM.displayStatesColored(rightScrollPane, graphViz, draw, Set.of(state), rightAlphabet);
                        descriptionText.setText(descriptionText.getText()
                                .concat("Added state ")
                                .concat(state)
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);
                        break;

                    case 3:
                        var transitionNew = (Pair<Triple<String, Character, String>, Triple<String, Character, String>>)iterator.next();
                        var first = transitionNew.getFirst();
                        var second = transitionNew.getSecond();

                        draw.addTransition(second.getFirst(), second.getSecond(), second.getThird());

                        descriptionText.setText(descriptionText.getText()
                                .concat("Added transition from: ")
                                .concat(second.getFirst())
                                .concat(" on \'")
                                .concat(second.getSecond().toString())
                                .concat("\' to: ")
                                .concat(second.getThird())
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);

                        RunFSM.displayTransitionsColored(leftScrollPane, graphViz, machine, Set.of(first, new Triple<>(second.getFirst(), '#', first.getThird())), leftAlphabet);
                        RunFSM.displayTransitionsColored(rightScrollPane, graphViz, draw, Set.of(second), rightAlphabet);
                        break;

                    case 4:
                        var transitionOld = (Triple<String, Character, String>)iterator.next();

                        draw.addTransition(transitionOld.getFirst(), transitionOld.getSecond(), transitionOld.getThird());

                        descriptionText.setText(descriptionText.getText()
                                .concat("Added transition from: ")
                                .concat(transitionOld.getFirst())
                                .concat(" on \'")
                                .concat(transitionOld.getSecond().toString())
                                .concat("\' to: ")
                                .concat(transitionOld.getThird())
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);

                        RunFSM.displayTransitionsColored(leftScrollPane, graphViz, machine, Set.of(transitionOld), leftAlphabet);
                        RunFSM.displayTransitionsColored(rightScrollPane, graphViz, draw, Set.of(transitionOld), rightAlphabet);
                        break;

                    case 5:
                        var initial = (String)iterator.next();
                        draw.setInitial(initial);

                        descriptionText.setText(descriptionText.getText()
                                .concat("Set initial state to: ")
                                .concat(draw.getInitial()).concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);

                        RunFSM.displayStatesColored(leftScrollPane, graphViz, machine, Set.of(machine.getInitial()), leftAlphabet);
                        RunFSM.displayStatesColored(rightScrollPane, graphViz, draw, Set.of(draw.getInitial()), rightAlphabet);
                        break;

                    case 6:
                        var finalState = (String)iterator.next();
                        draw.addStateFinal(finalState);

                        RunFSM.displayStatesColored(leftScrollPane, graphViz, machine, Set.of(finalState), leftAlphabet);
                        RunFSM.displayStatesColored(rightScrollPane, graphViz, draw, Set.of(finalState), rightAlphabet);

                        descriptionText.setText(descriptionText.getText()
                                .concat("Marked state ")
                                .concat(finalState)
                                .concat(" as final")
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);
                        break;

                    case 7:
                        var anyFinal = (String)iterator.next();
                        draw.addStateFinal(anyFinal);

                        descriptionText.setText(descriptionText.getText()
                                .concat("Marked state ")
                                .concat(machine.getInitial())
                                .concat(" as final")
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);

                        RunFSM.displayStatesColored(leftScrollPane, graphViz, machine, Set.of(anyFinal), leftAlphabet);
                        RunFSM.displayStatesColored(rightScrollPane, graphViz, draw, Set.of(anyFinal), rightAlphabet);
                        break;
                }
                break;


            case Piggy:
                switch (step) {
                    case 0:
                        iterator.next();
                        machine = new NormalFormFactory().get(NormalFormType.EpsilonFree).apply(machine);
                        BuildFSM.displayDiagramAndAlphabet(leftScrollPane, graphViz, machine, leftAlphabet);
                        descriptionText.setText(descriptionText.getText().concat("Constructed nfa to its epsilon-free normal form").concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);
                        break;

                    case 1:
                        var symbol = (Character)iterator.next();
                        draw.setAlphabet(Sets.union(draw.getAlphabet(), Set.of(symbol)));
                        BuildFSM.displayDiagramAndAlphabet(rightScrollPane, graphViz, draw, rightAlphabet);
                        descriptionText.setText(descriptionText.getText()
                                .concat("Added symbol \'")
                                .concat(symbol.toString())
                                .concat("\' to alphabet")
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);
                        break;

                    case 2:
                        var state = (String)iterator.next();
                        draw.addState(state);

                        RunFSM.displayStatesColored(leftScrollPane, graphViz, machine, Set.of(state), leftAlphabet);
                        RunFSM.displayStatesColored(rightScrollPane, graphViz, draw, Set.of(state), rightAlphabet);
                        descriptionText.setText(descriptionText.getText()
                                .concat("Added state ")
                                .concat(state)
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);
                        break;

                    case 3:
                        var newState = (String)iterator.next();
                        draw.addState(newState);

                        RunFSM.displayStatesColored(leftScrollPane, graphViz, machine, Set.of(newState), leftAlphabet);
                        RunFSM.displayStatesColored(rightScrollPane, graphViz, draw, Set.of(newState), rightAlphabet);
                        descriptionText.setText(descriptionText.getText()
                                .concat("Added state ")
                                .concat(newState)
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);
                        break;

                    case 4:
                        var transitionOld = (Triple<String, Character, String>)iterator.next();

                        draw.addTransition(transitionOld.getFirst(), transitionOld.getSecond(), transitionOld.getThird());

                        descriptionText.setText(descriptionText.getText()
                                .concat("Added transition from: ")
                                .concat(transitionOld.getFirst())
                                .concat(" on \'")
                                .concat(transitionOld.getSecond().toString())
                                .concat("\' to: ")
                                .concat(transitionOld.getThird())
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);

                        RunFSM.displayTransitionsColored(leftScrollPane, graphViz, machine, Set.of(transitionOld), leftAlphabet);
                        RunFSM.displayTransitionsColored(rightScrollPane, graphViz, draw, Set.of(transitionOld), rightAlphabet);
                        break;

                    case 5:
                        var transitionsNew = (Triple<String, Character, String>)iterator.next();

                        draw.addTransition(transitionsNew.getFirst(), transitionsNew.getSecond(), transitionsNew.getThird());

                        descriptionText.setText(descriptionText.getText()
                                .concat("Added transition from: ")
                                .concat(transitionsNew.getFirst())
                                .concat(" on \'")
                                .concat(transitionsNew.getSecond().toString())
                                .concat("\' to: ")
                                .concat(transitionsNew.getThird())
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);

                        var leftState = transitionsNew.getThird().equals(machine.getInitial()) ? transitionsNew.getThird() : transitionsNew.getFirst();

                        RunFSM.displayStatesColored(leftScrollPane, graphViz, machine, Set.of(leftState), leftAlphabet);
                        RunFSM.displayTransitionsColored(rightScrollPane, graphViz, draw, Set.of(transitionsNew), rightAlphabet);
                        break;

                    case 6:
                        var initial = (String)iterator.next();
                        draw.setInitial(initial);

                        descriptionText.setText(descriptionText.getText()
                                .concat("Set initial state to: ")
                                .concat(draw.getInitial()).concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);

                        RunFSM.displayStatesColored(leftScrollPane, graphViz, machine, Set.of(), leftAlphabet);
                        RunFSM.displayStatesColored(rightScrollPane, graphViz, draw, Set.of(draw.getInitial()), rightAlphabet);
                        break;

                    case 7:
                        var finalState = (String)iterator.next();
                        draw.addStateFinal(finalState);

                        descriptionText.setText(descriptionText.getText()
                                .concat("Marked state ")
                                .concat(finalState)
                                .concat(" as final")
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);

                        RunFSM.displayStatesColored(leftScrollPane, graphViz, machine, Set.of(), leftAlphabet);
                        RunFSM.displayStatesColored(rightScrollPane, graphViz, draw, Set.of(finalState), rightAlphabet);
                        break;
                }
                break;


            case Reachable:
                switch (step) {
                    case 0:
                        iterator.next();
                        break;

                    case 1:
                        var state = (String)iterator.next();
                        draw.addState(state);

                        RunFSM.displayStatesColored(leftScrollPane, graphViz, machine, Set.of(state), leftAlphabet);
                        RunFSM.displayStatesColored(leftScrollPane, graphViz, machine, draw.getStates(), leftAlphabet);
                        descriptionText.setText(descriptionText.getText()
                                .concat("Marked state ")
                                .concat(state)
                                .concat(" as unreachable")
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);
                        break;

                    case 2:
                        var newState = (String)iterator.next();
                        draw.removeState(newState);

                        RunFSM.displayStatesColored(leftScrollPane, graphViz, machine, Set.of(newState), leftAlphabet);
                        RunFSM.displayStatesColored(rightScrollPane, graphViz, draw, Set.of(), rightAlphabet);
                        descriptionText.setText(descriptionText.getText()
                                .concat("Removed state ")
                                .concat(newState)
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);
                        break;
                }
                break;
        }
    }

    void runNFFSMConstruction(FSM machine, boolean direct) {
        this.machine = machine.copy();
        this.direct = direct;
        normalForm = new NormalFormFactory().get(NFControllerMediator.getInstance().getType());
        steps = normalForm.getSteps();
        resultMachine = normalForm.apply(machine);
        draw = new NFA("draw");
        algorithmLength = steps.size() - 1;
        step = 0;
        iterator = Set.of("convert").iterator();
        descriptionText.setText(steps.get(step).concat("\n"));
        descriptionScrollPane.setVvalue(1.0);
        BuildFSM.displayDiagramAndAlphabet(leftScrollPane, graphViz, machine, leftAlphabet);
    }
}
