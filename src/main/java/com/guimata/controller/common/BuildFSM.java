package com.guimata.controller.common;

import com.github.jabbalaci.graphviz.GraphViz;
import com.jfoenix.controls.*;
import com.libfsm.automata.machines.DFA;
import com.libfsm.automata.machines.FSM;
import com.libfsm.automata.machines.FSMType;
import com.libfsm.automata.machines.NFA;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.IOException;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class BuildFSM {
    public static void addState(StackPane root, ScrollPane scrollPane, GraphViz graphViz, FSM machine, Text alphabetText) {
        JFXDialogLayout content = new JFXDialogLayout();
        VBox inputItems = new VBox(10);

        JFXTextField nameField = new JFXTextField();
        nameField.setLabelFloat(true);
        nameField.setPromptText("Name");
        JFXCheckBox isFinal = new JFXCheckBox();
        isFinal.setText("Final state");
        inputItems.getChildren().addAll(nameField, isFinal);
        content.setBody(inputItems);

        JFXButton cancel = new JFXButton("Cancel");
        cancel.setButtonType(JFXButton.ButtonType.RAISED);
        JFXButton add = new JFXButton("Add");
        add.setButtonType(JFXButton.ButtonType.RAISED);
        content.setActions(cancel, add);

        JFXDialog dialog = new JFXDialog(root, content, JFXDialog.DialogTransition.CENTER);
        content.setPrefWidth(200);
        dialog.setPrefWidth(200);
        cancel.setOnAction(e -> dialog.close());
        add.setOnAction(e -> {
            String stateName = nameField.getText();
            boolean finalState = isFinal.isSelected();

            if (!stateName.isEmpty()) {
                machine.addState(stateName);
                if (finalState)
                    machine.addStateFinal(stateName);
                displayDiagramAndAlphabet(scrollPane, graphViz, machine, alphabetText);
            }
            dialog.close();
        });

        dialog.show();
    }

    public static void removeState(StackPane root, ScrollPane scrollPane, GraphViz graphViz, FSM machine, Text alphabetText) {
        JFXDialogLayout content = new JFXDialogLayout();
        VBox inputItems = new VBox(10);

        JFXTextField nameField = new JFXTextField();
        nameField.setLabelFloat(true);
        nameField.setPromptText("Name");
        inputItems.getChildren().addAll(nameField);
        content.setBody(inputItems);

        JFXButton cancel = new JFXButton("Cancel");
        cancel.setButtonType(JFXButton.ButtonType.RAISED);
        JFXButton remove = new JFXButton("Remove");
        remove.setButtonType(JFXButton.ButtonType.RAISED);
        content.setActions(cancel, remove);

        JFXDialog dialog = new JFXDialog(root, content, JFXDialog.DialogTransition.CENTER);
        content.setPrefWidth(200);
        dialog.setPrefWidth(200);
        cancel.setOnAction(e -> dialog.close());
        remove.setOnAction(e -> {
            String state = nameField.getText();

            machine.removeStateFinal(state);
            machine.removeState(state);

            if (machine.getInitial().equals(state))
                machine.setInitial("");
            displayDiagramAndAlphabet(scrollPane, graphViz, machine, alphabetText);
            dialog.close();
        });
        dialog.show();
    }

    public static void renameState(StackPane root, ScrollPane scrollPane, GraphViz graphViz, FSM machine, Text alphabetText) {
        JFXDialogLayout content = new JFXDialogLayout();
        VBox inputItems = new VBox(20);

        JFXTextField oldNameField = new JFXTextField();
        oldNameField.setLabelFloat(true);
        oldNameField.setPromptText("Old name");
        JFXTextField newNameField = new JFXTextField();
        newNameField.setLabelFloat(true);
        newNameField.setPromptText("New name");
        inputItems.getChildren().addAll(oldNameField, newNameField);
        content.setBody(inputItems);

        JFXButton cancel = new JFXButton("Cancel");
        cancel.setButtonType(JFXButton.ButtonType.RAISED);
        JFXButton rename = new JFXButton("Rename");
        rename.setButtonType(JFXButton.ButtonType.RAISED);
        content.setActions(cancel, rename);

        JFXDialog dialog = new JFXDialog(root, content, JFXDialog.DialogTransition.CENTER);
        content.setPrefWidth(200);
        dialog.setPrefWidth(200);
        cancel.setOnAction(e -> dialog.close());
        rename.setOnAction(e -> {
            String oldName = oldNameField.getText();
            String newName = newNameField.getText();
            machine.renameState(oldName, newName);
            displayDiagramAndAlphabet(scrollPane, graphViz, machine, alphabetText);
            dialog.close();
        });
        dialog.show();
    }

    public static void addTransition(StackPane root, ScrollPane scrollPane, GraphViz graphViz, FSM machine, Text alphabetText) {
        JFXDialogLayout content = new JFXDialogLayout();
        VBox inputItems = new VBox(20);

        JFXTextField source = new JFXTextField();
        source.setLabelFloat(true);
        source.setPromptText("Source");
        JFXTextField symbol = new JFXTextField();
        symbol.setLabelFloat(true);
        symbol.setPromptText("Symbol");
        JFXTextField destination = new JFXTextField();
        destination.setLabelFloat(true);
        destination.setPromptText("Destination");
        inputItems.getChildren().addAll(source, symbol, destination);
        content.setBody(inputItems);

        JFXButton cancel = new JFXButton("Cancel");
        cancel.setButtonType(JFXButton.ButtonType.RAISED);
        JFXButton add = new JFXButton("Add");
        add.setButtonType(JFXButton.ButtonType.RAISED);
        content.setActions(cancel, add);

        JFXDialog dialog = new JFXDialog(root, content, JFXDialog.DialogTransition.CENTER);
        content.setPrefWidth(200);
        dialog.setPrefWidth(200);
        cancel.setOnAction(e -> dialog.close());
        add.setOnAction(e -> {
            String sourceState = source.getText();
            String symbolChar = symbol.getText();

            if (symbolChar.length() != 1)
                return;

            Character character = symbolChar.charAt(0);
            String destinationState = destination.getText();
            machine.addTransition(sourceState, character, destinationState);
            displayDiagramAndAlphabet(scrollPane, graphViz, machine, alphabetText);
            dialog.close();
        });
        dialog.show();
    }

    public static void removeTransition(StackPane root, ScrollPane scrollPane, GraphViz graphViz, FSM machine, Text alphabetText) {
        JFXDialogLayout content = new JFXDialogLayout();
        VBox inputItems = new VBox(20);

        JFXTextField source = new JFXTextField();
        source.setLabelFloat(true);
        source.setPromptText("Source");
        JFXTextField symbol = new JFXTextField();
        symbol.setLabelFloat(true);
        symbol.setPromptText("Symbol");
        JFXTextField destination = new JFXTextField();
        destination.setLabelFloat(true);
        destination.setPromptText("Destination");
        inputItems.getChildren().addAll(source, symbol, destination);
        content.setBody(inputItems);

        JFXButton cancel = new JFXButton("Cancel");
        cancel.setButtonType(JFXButton.ButtonType.RAISED);
        JFXButton remove = new JFXButton("Remove");
        remove.setButtonType(JFXButton.ButtonType.RAISED);
        content.setActions(cancel, remove);

        JFXDialog dialog = new JFXDialog(root, content, JFXDialog.DialogTransition.CENTER);
        content.setPrefWidth(200);
        dialog.setPrefWidth(200);
        cancel.setOnAction(e -> dialog.close());
        remove.setOnAction(e -> {
            String sourceState = source.getText();
            String symbolChar = symbol.getText();

            if (symbolChar.length() != 1)
                return;

            Character character = symbolChar.charAt(0);
            String destinationState = destination.getText();
            machine.removeTransition(sourceState, character, destinationState);
            displayDiagramAndAlphabet(scrollPane, graphViz, machine, alphabetText);
            dialog.close();
        });
        dialog.show();
    }

    public static void defineAlphabet(StackPane root, ScrollPane scrollPane, GraphViz graphViz, FSM machine, Text alphabetText) {
        JFXDialogLayout content = new JFXDialogLayout();
        VBox inputItems = new VBox(20);

        JFXTextField alphabet = new JFXTextField();
        alphabet.setLabelFloat(true);
        alphabet.setPromptText("Alphanum separated by whatever");

        inputItems.getChildren().addAll(alphabet);
        content.setBody(inputItems);

        JFXButton cancel = new JFXButton("Cancel");
        cancel.setButtonType(JFXButton.ButtonType.RAISED);
        JFXButton okButton = new JFXButton("Ok");
        okButton.setButtonType(JFXButton.ButtonType.RAISED);
        content.setActions(cancel, okButton);

        JFXDialog dialog = new JFXDialog(root, content, JFXDialog.DialogTransition.CENTER);
        content.setPrefWidth(300);
        dialog.setPrefWidth(300);
        cancel.setOnAction(e -> dialog.close());
        okButton.setOnAction(e -> {
            JFXDialogLayout warningContent = new JFXDialogLayout();
            warningContent.setBody(new Text("This will remove all transitions of missing symbols."));

            JFXButton warningCancel = new JFXButton("Cancel");
            warningCancel.setButtonType(JFXButton.ButtonType.RAISED);
            JFXButton warningOk = new JFXButton("Ok");
            warningOk.setButtonType(JFXButton.ButtonType.RAISED);
            warningContent.setActions(warningCancel, warningOk);

            JFXDialog warningDialog = new JFXDialog(root, warningContent, JFXDialog.DialogTransition.BOTTOM);
            warningContent.setPrefWidth(200);
            warningDialog.setPrefWidth(200);
            warningDialog.setOverlayClose(false);
            warningCancel.setOnAction(x -> warningDialog.close());
            warningOk.setOnAction(x -> {
                String inputAlphabet = alphabet.getText();
                Set<Character> chars = inputAlphabet
                        .replaceAll("[^A-Za-z0-9]","")
                        .chars()
                        .mapToObj(c -> (char)c)
                        .collect(Collectors.toSet());

                machine.setAlphabet(chars);

                displayDiagramAndAlphabet(scrollPane, graphViz, machine, alphabetText);
                warningDialog.close();
                dialog.close();
            });
            warningDialog.show();
        });
        dialog.show();
    }

    public static void clearAutomaton(ScrollPane scrollPane, GraphViz graphViz, FSM machine, Text alphabetText) {
        if (machine.getFsmType().equals(FSMType.DFA))
            machine = new DFA("empty");
        else
            machine = new NFA("empty");

        displayDiagramAndAlphabet(scrollPane, graphViz, machine, alphabetText);
    }

    public static void setInitial(StackPane root, ScrollPane scrollPane, GraphViz graphViz, FSM machine, Text alphabetText) {
        JFXDialogLayout content = new JFXDialogLayout();
        VBox inputItems = new VBox(10);

        JFXTextField nameField = new JFXTextField();
        nameField.setLabelFloat(true);
        nameField.setPromptText("Name");
        inputItems.getChildren().addAll(nameField);
        content.setBody(inputItems);

        JFXButton cancel = new JFXButton("Cancel");
        cancel.setButtonType(JFXButton.ButtonType.RAISED);
        JFXButton set = new JFXButton("Set");
        set.setButtonType(JFXButton.ButtonType.RAISED);
        content.setActions(cancel, set);

        JFXDialog dialog = new JFXDialog(root, content, JFXDialog.DialogTransition.CENTER);
        content.setPrefWidth(200);
        dialog.setPrefWidth(200);
        cancel.setOnAction(e -> dialog.close());
        set.setOnAction(e -> {
            String state = nameField.getText();

            if (machine.getStates().contains(state)) {
                machine.setInitial(state);
                displayDiagramAndAlphabet(scrollPane, graphViz, machine, alphabetText);
            }
            dialog.close();
        });
        dialog.show();
    }

    public static void exportToFile(FSM machine) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialFileName("machine.json");
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("JSON", "*.json"));
        Optional<File> selectedFile = Optional.ofNullable(fileChooser.showSaveDialog(null));

        if (selectedFile.isPresent() && selectedFile.get().getName().endsWith(".json"))
            machine.writeToFile(selectedFile.get());
    }

    public static void renameFSM(StackPane root, ScrollPane scrollPane, GraphViz graphViz, FSM machine, Text alphabetText) {
        JFXDialogLayout content = new JFXDialogLayout();
        VBox inputItems = new VBox(20);

        Text oldNameText = new Text("Current: ".concat(machine.getName()));
        JFXTextField newNameField = new JFXTextField();
        newNameField.setLabelFloat(true);
        newNameField.setPromptText("New name");
        inputItems.getChildren().addAll(oldNameText, newNameField);
        content.setBody(inputItems);

        JFXButton cancel = new JFXButton("Cancel");
        cancel.setButtonType(JFXButton.ButtonType.RAISED);
        JFXButton rename = new JFXButton("Rename");
        rename.setButtonType(JFXButton.ButtonType.RAISED);
        content.setActions(cancel, rename);

        JFXDialog dialog = new JFXDialog(root, content, JFXDialog.DialogTransition.CENTER);
        content.setPrefWidth(200);
        dialog.setPrefWidth(200);
        cancel.setOnAction(e -> dialog.close());
        rename.setOnAction(e -> {
            String newName = newNameField.getText();
            machine.setName(newName);
            displayDiagramAndAlphabet(scrollPane, graphViz, machine, alphabetText);
            dialog.close();
        });
        dialog.show();
    }


    public static void displayDiagramAndAlphabet(ScrollPane scrollPane, GraphViz graphViz, FSM machine, Text alphabetText) {
        graphViz.clearGraph();
        graphViz.addln(graphViz.start_graph());
        graphViz.addln("rankdir=LR;");
        graphViz.addln("init [shape = none, label = \"\"];");

        machine.getStates().forEach(q -> {
            String typeState = machine.isFinalState(q) ? " [shape = doublecircle];" : " [shape = circle];";
            graphViz.addln("\""+q+"\""+typeState);
        });

        if (!machine.getInitial().isEmpty())
            graphViz.addln("init -> "+"\""+ machine.getInitial()+"\""+";");

        machine.getStates().forEach(p -> {
            machine.getStates().forEach(q -> {
                Set<String> chars = machine.getAllTransitionsFromTo(p, q)
                        .stream()
                        .map(Object::toString)
                        .collect(Collectors.toSet());
                if (!chars.isEmpty()) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("\"").append(p).append("\" -> \"").append(q).append("\"");
                    sb.append(" [label = <").append(String.join(" , ", chars)).append(">];");
                    graphViz.addln(sb.toString());
                }
            });
        });

        graphViz.addln(graphViz.end_graph());

        updateDiagram(graphViz, scrollPane);

        alphabetText.setText(String.join(",", machine.getAlphabet()
                .stream()
                .map(Object::toString)
                .collect(Collectors.toSet())));
    }

    public static void updateDiagram(GraphViz graphViz, ScrollPane scrollPane) {
        String type = "png";
        String represantionType = "dot";
        //File out = new File("tmp/machine."+type);

        File out = null;
        try {
            out = File.createTempFile("machine", type);
            graphViz.writeGraphToFile(graphViz.getGraph(graphViz.getDotSource(), type, represantionType), out);
            Image diagramImage = new Image(out.toURI().toString());
            ImageView diagramView = new ImageView(diagramImage);
            StackPane imageHolder = new StackPane(diagramView);
            imageHolder.setStyle("-fx-background-color: #ffffff");
            imageHolder.setMinWidth(scrollPane.getPrefWidth());
            imageHolder.setMinHeight(scrollPane.getPrefHeight());
            scrollPane.setContent(imageHolder);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void displayInfoDialog(StackPane root, String s) {
        JFXDialogLayout content = new JFXDialogLayout();
        Text text = new Text(s);
        text.setLineSpacing(1);
        content.setBody(text);

        JFXButton ok = new JFXButton("Ok");
        ok.setButtonType(JFXButton.ButtonType.RAISED);
        content.setActions(ok);

        JFXDialog dialog = new JFXDialog(root, content, JFXDialog.DialogTransition.BOTTOM);
        content.setPrefWidth(200);
        dialog.setPrefWidth(200);
        dialog.setOverlayClose(false);
        ok.setOnAction(e -> dialog.close());
        dialog.show();
    }
}
