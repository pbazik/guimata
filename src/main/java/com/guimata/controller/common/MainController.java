package com.guimata.controller.common;

import com.guimata.controller.computation.CControllerMediator;

import com.guimata.controller.equivalence.EqControllerMediator;
import com.guimata.controller.normalForms.NFControllerMediator;
import com.guimata.controller.operations.OpControllerMediator;
import com.jfoenix.controls.JFXTabPane;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;


import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable {

    @FXML
    private AnchorPane root;

    @FXML
    private StackPane cStackPane;

    @FXML
    private StackPane nfStackPane;

    @FXML
    private StackPane eqStackPane;

    @FXML
    private StackPane opStackPane;

    @FXML
    private JFXTabPane tabPane;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        CControllerMediator.getInstance().setMainController(this);
        NFControllerMediator.getInstance().setMainController(this);
        EqControllerMediator.getInstance().setMainController(this);
        OpControllerMediator.getInstance().setMainController(this);

        CControllerMediator.getInstance().loadCHome();
        NFControllerMediator.getInstance().loadNFHome();
        EqControllerMediator.getInstance().loadEqHome();
        OpControllerMediator.getInstance().loadOpHome();
    }

    public StackPane getcStackPane() {
        return cStackPane;
    }

    public StackPane getOpStackPane() {
        return opStackPane;
    }

    public StackPane getNfStackPane() {
        return nfStackPane;
    }

    public StackPane getEqStackPane() {
        return eqStackPane;
    }
}
