package com.guimata.controller.common;

import com.github.jabbalaci.graphviz.GraphViz;
import com.libfsm.automata.helpers.Pair;
import com.libfsm.automata.helpers.Triple;
import com.libfsm.automata.machines.FSM;
import javafx.scene.control.ScrollPane;
import javafx.scene.text.Text;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import static com.guimata.controller.common.BuildFSM.updateDiagram;

public class RunFSM {
    public static String stateFromSubset(Set<String> subset) {
        var result = new StringBuilder();
        result.append("{");
        result.append(String.join(", ", subset));
        result.append("}");
        return result.toString();
    }

    public static Set<String> subsetFromState(String state) {
        var temp = state.replace("{", "")
                .replace("}", "");

        return Arrays.stream(temp.split(", "))
                .collect(Collectors.toSet());
    }

    public static Pair<String, String> pairFromState(String state) {
        var temp = state.replace("[", "")
                .replace("]", "");

         var list = Arrays.stream(temp.split(", "))
                .collect(Collectors.toList());

         if (list.size() < 2)
             return new Pair<>("", "");

        return new Pair<>(list.get(0), list.get(1));
    }

    public static void displayStatesColored(ScrollPane scrollPane, GraphViz graphViz, FSM fsm, Set<String> states, Text alphabet) {
        graphViz.clearGraph();
        graphViz.addln(graphViz.start_graph());
        graphViz.addln("rankdir=LR;");
        graphViz.addln("init [shape = none, label = \"\"];");

        fsm.getStates().forEach(q -> {
            String colored = states.contains(q) ? "style = filled, fillcolor = \"#50A14F\", " : "";
            String typeState = fsm.isFinalState(q) ? "shape = doublecircle];" : "shape = circle];";
            graphViz.addln("\""+q+"\"" + " [" + colored + typeState);
        });

        if (!fsm.getInitial().isEmpty())
            graphViz.addln("init -> " + "\""+ fsm.getInitial() +"\""+ ";");

        fsm.getStates().forEach(p -> {
            fsm.getStates().forEach(q -> {
                Set<String> chars = fsm.getAllTransitionsFromTo(p, q)
                        .stream()
                        .map(Object::toString)
                        .collect(Collectors.toSet());

                if (!chars.isEmpty()) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("\"").append(p).append("\" -> \"").append(q).append("\"");

                    sb.append(" [label = <").append(String.join(" , ", chars)).append(">];");

                    graphViz.addln(sb.toString());
                }
            });
        });

        graphViz.addln(graphViz.end_graph());

        updateDiagram(graphViz, scrollPane);

        alphabet.setText(String.join(",", fsm.getAlphabet()
                .stream()
                .map(Object::toString)
                .collect(Collectors.toSet())));
    }

    public static void displayTransitionsColored(ScrollPane scrollPane, GraphViz graphViz, FSM fsm, Set<String> sources, Character symbol, Set<String> destinations, Text alphabet) {
        graphViz.clearGraph();
        graphViz.addln(graphViz.start_graph());
        graphViz.addln("rankdir=LR;");
        graphViz.addln("init [shape = none, label = \"\"];");

        fsm.getStates().forEach(q -> {
            String color = sources.contains(q) ?
                    "style = filled, fillcolor = \"#50A14F\", " : destinations.contains(q) ?
                    "style = filled, fillcolor = \"#4078F2\", " : "";

            color = (sources.contains(q) && destinations.contains(q)) ? "style = filled, fillcolor = \"#986801\", " : color;

            String typeState = fsm.isFinalState(q) ? "shape = doublecircle];" : "shape = circle];";
            graphViz.addln("\""+q+"\"" + " [" + color + typeState);
        });

        if (!fsm.getInitial().isEmpty())
            graphViz.addln("init -> " + "\""+ fsm.getInitial() +"\""+ ";");

        fsm.getStates().forEach(p -> {
            fsm.getStates().forEach(q -> {
                Set<String> chars = fsm.getAllTransitionsFromTo(p, q)
                        .stream()
                        .map(Object::toString)
                        .collect(Collectors.toSet());

                if (!chars.isEmpty()) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("\"").append(p).append("\" -> \"").append(q).append("\"");

                    sb.append(" [label = <");
                    var charIt = chars.iterator();
                    boolean any = false;
                    String current = charIt.next();

                    if (sources.contains(p) && current.equals(symbol.toString())) {
                        any = true;
                        sb.append("<font color=\"#CA1243\">").append(current).append("</font>");
                    } else {
                        sb.append(current);
                    }

                    while (charIt.hasNext()) {
                        current = charIt.next();
                        sb.append(" , ");
                        if (sources.contains(p) && current.equals(symbol.toString())) {
                            any = true;
                            sb.append("<font color=\"#CA1243\">").append(current).append("</font>");
                        } else {
                            sb.append(current);
                        }
                    }
                    sb.append(">");

                    if (any)
                        sb.append(", color = \"#CA1243\"");

                    sb.append("];");

                    graphViz.addln(sb.toString());
                }
            });
        });

        graphViz.addln(graphViz.end_graph());

        updateDiagram(graphViz, scrollPane);

        alphabet.setText(String.join(",", fsm.getAlphabet()
                .stream()
                .map(Object::toString)
                .collect(Collectors.toSet())));
    }

    public static void displayTransitionsColored(ScrollPane scrollPane, GraphViz graphViz, FSM fsm, Set<Triple<String, Character, String>> transitions, Text alphabet) {
        graphViz.clearGraph();
        graphViz.addln(graphViz.start_graph());
        graphViz.addln("rankdir=LR;");
        graphViz.addln("init [shape = none, label = \"\"];");

        var sources = new HashSet<String>();
        var destinations = new HashSet<String>();

        transitions.forEach(t -> {
            sources.add(t.getFirst());
            destinations.add(t.getThird());
        });

        fsm.getStates().forEach(q -> {
            String color = sources.contains(q) ?
                    "style = filled, fillcolor = \"#50A14F\", " : destinations.contains(q) ?
                    "style = filled, fillcolor = \"#4078F2\", " : "";

            color = (sources.contains(q) && destinations.contains(q)) ? "style = filled, fillcolor = \"#986801\", " : color;

            String typeState = fsm.isFinalState(q) ? "shape = doublecircle];" : "shape = circle];";
            graphViz.addln("\""+q+"\"" + " [" + color + typeState);
        });

        if (!fsm.getInitial().isEmpty())
            graphViz.addln("init -> " + "\""+ fsm.getInitial() +"\""+ ";");

        fsm.getStates().forEach(p -> {
            fsm.getStates().forEach(q -> {
                Set<Character> chars = fsm.getAllTransitionsFromTo(p, q);

                if (!chars.isEmpty()) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("\"").append(p).append("\" -> \"").append(q).append("\"");

                    sb.append(" [label = <");
                    var charIt = chars.iterator();
                    boolean any = false;
                    Character current = charIt.next();

                    if (transitions.contains(new Triple<>(p, current, q))) {
                        any = true;
                        sb.append("<font color=\"#CA1243\">").append(current.toString()).append("</font>");
                    } else {
                        sb.append(current.toString());
                    }

                    while (charIt.hasNext()) {
                        current = charIt.next();
                        sb.append(" , ");
                        if (transitions.contains(new Triple<>(p, current, q))) {
                            any = true;
                            sb.append("<font color=\"#CA1243\">").append(current.toString()).append("</font>");
                        } else {
                            sb.append(current.toString());
                        }
                    }
                    sb.append(">");

                    if (any)
                        sb.append(", color = \"#CA1243\"");

                    sb.append("];");

                    graphViz.addln(sb.toString());
                }
            });
        });

        graphViz.addln(graphViz.end_graph());

        updateDiagram(graphViz, scrollPane);

        alphabet.setText(String.join(",", fsm.getAlphabet()
                .stream()
                .map(Object::toString)
                .collect(Collectors.toSet())));
    }
}
