package com.guimata.controller.equivalence;

import com.github.jabbalaci.graphviz.GraphViz;
import com.guimata.controller.common.BuildFSM;
import com.guimata.controller.common.RunFSM;
import com.jfoenix.controls.JFXButton;
import com.libfsm.automata.helpers.Sets;
import com.libfsm.automata.helpers.Triple;
import com.libfsm.automata.machines.DFA;
import com.libfsm.automata.machines.FSM;
import com.libfsm.conversions.NFAtoDFA;
import com.libfsm.normalforms.common.NormalFormFactory;
import com.libfsm.normalforms.common.NormalFormType;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.*;

import static com.guimata.controller.common.RunFSM.stateFromSubset;

public class EqRunFSMController implements Initializable {
    private NFAtoDFA conversion;
    private Map<Integer, String> steps = new HashMap<>();
    private GraphViz graphViz = new GraphViz();
    private FSM machine;
    private FSM resultMachine;
    private FSM draw;
    private boolean direct = false;
    private int algorithmLength;
    private int step;
    private Iterator iterator;

    @FXML
    private VBox leftVbox;

    @FXML
    private ScrollPane leftScrollPane;

    @FXML
    private Text leftAlphabet;

    @FXML
    private VBox rightVbox;

    @FXML
    private ScrollPane rightScrollPane;

    @FXML
    private Text rightAlphabet;

    @FXML
    private JFXButton nextButton;

    @FXML
    private Text descriptionText;

    @FXML
    private ScrollPane descriptionScrollPane;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        EqControllerMediator.getInstance().setEqRunFSMController(this);
        HBox.setHgrow(leftVbox, Priority.ALWAYS);
        HBox.setHgrow(rightVbox, Priority.ALWAYS);
        VBox.setVgrow(leftScrollPane, Priority.ALWAYS);
        VBox.setVgrow(rightScrollPane, Priority.ALWAYS);
        descriptionScrollPane.setVvalue(1.0);
    }

    @FXML
    void changeToFSMBuild(ActionEvent event) {
        EqControllerMediator.getInstance().loadEqBuildFSM(machine);
    }

    @FXML
    void nextStep(ActionEvent event) {
        if (direct) {
            machine = new NFAtoDFA().apply(new NormalFormFactory().get(NormalFormType.EpsilonFree).apply(machine));
            BuildFSM.displayInfoDialog(EqControllerMediator.getInstance().getRoot(), "Completed");
            BuildFSM.displayDiagramAndAlphabet(rightScrollPane, graphViz, machine, rightAlphabet);
            nextButton.setDisable(true);
            return;
        }

        if (step >= algorithmLength) {
            machine = new NFAtoDFA().apply(new NormalFormFactory().get(NormalFormType.EpsilonFree).apply(machine));
            BuildFSM.displayInfoDialog(EqControllerMediator.getInstance().getRoot(), "Completed");
            descriptionScrollPane.setVvalue(1.0);
            nextButton.setDisable(true);
            return;
        }

        if (!iterator.hasNext()) {
            step++;

            switch (step) {
                case 1:
                    iterator = machine.getAlphabet().iterator();
                    break;

                case 2:
                    var states = new HashSet<String>();
                    Sets.powerSet(machine.getStates()).forEach(subset -> states.add(stateFromSubset(subset)));

                    iterator = states.iterator();
                    break;

                case 3:
                    var transitions = new HashSet<Triple<String, Character, String>>();

                    for (var subset : Sets.powerSet(machine.getStates())) {
                        var source = stateFromSubset(subset);

                        for (var symbol : machine.getAlphabet()) {

                            var destinations = new HashSet<>(machine.computeStep(subset, symbol));
                            var destination = stateFromSubset(destinations);

                            transitions.add(new Triple<>(source, symbol, destination));
                        }
                    }
                    iterator = transitions.iterator();
                    break;

                case 4:
                    iterator = Set.of(stateFromSubset(Set.of(machine.getInitial()))).iterator();
                    break;

                case 5:
                    var finalStates = new HashSet<String>();

                    for (var subset : Sets.powerSet(machine.getStates())) {
                        var source = stateFromSubset(subset);
                        if (subset.stream().anyMatch(machine::isFinalState))
                            finalStates.add(source);
                    }
                    iterator = finalStates.iterator();
                    break;
            }

            BuildFSM.displayDiagramAndAlphabet(leftScrollPane, graphViz, machine, leftAlphabet);
            BuildFSM.displayDiagramAndAlphabet(rightScrollPane, graphViz, draw, rightAlphabet);

            descriptionText.setText(descriptionText.getText().concat(steps.get(step)).concat("\n"));
            descriptionScrollPane.setVvalue(1.0);
            return;
        }

        switch (step) {
            case 0:
                iterator.next();
                machine = new NormalFormFactory().get(NormalFormType.EpsilonFree).apply(machine);
                BuildFSM.displayDiagramAndAlphabet(leftScrollPane, graphViz, machine, leftAlphabet);
                descriptionText.setText(descriptionText.getText().concat("Constructed machine to its epsilon-free normal form").concat("\n"));
                descriptionScrollPane.setVvalue(1.0);
                break;

            case 1:
                var symbol = (Character)iterator.next();
                draw.setAlphabet(Sets.union(draw.getAlphabet(), Set.of(symbol)));
                BuildFSM.displayDiagramAndAlphabet(rightScrollPane, graphViz, draw, rightAlphabet);
                descriptionText.setText(descriptionText.getText()
                        .concat("Added symbol \'")
                        .concat(symbol.toString())
                        .concat("\' to resultMachine's alphabet")
                        .concat("\n"));
                descriptionScrollPane.setVvalue(1.0);
                break;

            case 2:
                var state = (String)iterator.next();
                draw.addState(state);

                var subset = RunFSM.subsetFromState(state);

                RunFSM.displayStatesColored(leftScrollPane, graphViz, machine, subset, leftAlphabet);
                RunFSM.displayStatesColored(rightScrollPane, graphViz, draw, Set.of(state), rightAlphabet);

                descriptionText.setText(descriptionText.getText()
                        .concat("Added state ")
                        .concat(state)
                        .concat(" to resultMachine")
                        .concat("\n"));
                descriptionScrollPane.setVvalue(1.0);
                break;

            case 3:
                var transition = (Triple<String, Character, String>)iterator.next();
                var source = (String)transition.getFirst();
                var c = (Character)transition.getSecond();
                var destination = (String)transition.getThird();

                var sourceSubset = RunFSM.subsetFromState(source);
                var destinationSubset = RunFSM.subsetFromState(destination);

                draw.addTransition(source, c, destination);

                RunFSM.displayTransitionsColored(leftScrollPane, graphViz, machine, sourceSubset, c, destinationSubset, leftAlphabet);
                RunFSM.displayTransitionsColored(rightScrollPane, graphViz, draw, Set.of(transition), rightAlphabet);

                descriptionText.setText(descriptionText.getText()
                        .concat("Added transition from: ")
                        .concat(source)
                        .concat(" on \'")
                        .concat(c.toString())
                        .concat("\' to: ")
                        .concat(destination)
                        .concat("\n"));
                descriptionScrollPane.setVvalue(1.0);
                break;

            case 4:
                var initial = (String)iterator.next();
                draw.setInitial(initial);
                descriptionText.setText(descriptionText.getText()
                        .concat("Set initial state of resultMachine to: ")
                        .concat(initial).concat("\n"));
                descriptionScrollPane.setVvalue(1.0);

                RunFSM.displayStatesColored(leftScrollPane, graphViz, machine, Set.of(machine.getInitial()), leftAlphabet);
                RunFSM.displayStatesColored(rightScrollPane, graphViz, draw, Set.of(draw.getInitial()), rightAlphabet);
                break;

            case 5:
                var finalState = (String)iterator.next();
                draw.addStateFinal(finalState);

                var finalSubset = RunFSM.subsetFromState(finalState);

                RunFSM.displayStatesColored(leftScrollPane, graphViz, machine, finalSubset, leftAlphabet);
                RunFSM.displayStatesColored(rightScrollPane, graphViz, draw, Set.of(finalState), rightAlphabet);

                descriptionText.setText(descriptionText.getText()
                        .concat("Marked state ")
                        .concat(finalState)
                        .concat(" as final")
                        .concat("\n"));
                descriptionScrollPane.setVvalue(1.0);
                break;
        }
    }

    void runEqFSMConstruction(FSM machine, boolean direct) {
        this.machine = machine.copy();
        this.direct = direct;
        conversion = new NFAtoDFA();
        steps = conversion.getSteps();
        resultMachine = conversion.apply(machine);
        draw = new DFA("draw");
        algorithmLength = steps.size() - 1;
        step = 0;
        var convert = Set.of("convert");
        iterator = convert.iterator();
        descriptionText.setText(steps.get(step).concat("\n"));
        descriptionScrollPane.setVvalue(1.0);
        BuildFSM.displayDiagramAndAlphabet(leftScrollPane, graphViz, machine, leftAlphabet);
    }
}
