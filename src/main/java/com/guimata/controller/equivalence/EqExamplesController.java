package com.guimata.controller.equivalence;

import com.guimata.controller.computation.CControllerMediator;
import com.libfsm.automata.machines.FSM;
import com.libfsm.examples.dfa.DFAEx01;
import com.libfsm.examples.nfa.NFAEx01;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import java.net.URL;
import java.util.ResourceBundle;

public class EqExamplesController implements Initializable {

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        EqControllerMediator.getInstance().setEqExamplesController(this);
    }

    @FXML
    void changeToHome(ActionEvent event) {
        EqControllerMediator.getInstance().loadEqHome();
    }

    @FXML
    void endsWithAButton(ActionEvent event) {
        FSM machine = NFAEx01.get();
        EqControllerMediator.getInstance().loadEqBuildFSM(machine);
    }

    @FXML
    void numberAsButton(ActionEvent event) {
        FSM machine = DFAEx01.get();
        EqControllerMediator.getInstance().loadEqBuildFSM(machine);
    }

}
