package com.guimata.controller.equivalence;

import com.guimata.controller.common.MainController;
import com.libfsm.automata.machines.FSM;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;

import java.io.IOException;

public class EqControllerMediator {
    private MainController mainController;
    private EqHomeController eqHomeController;
    private EqBuildFSMController eqBuildFSMController;
    private EqRunFSMController eqRunFSMController;
    private EqExamplesController eqExamplesController;

    @FXML
    private StackPane root;

    public void setMainController(MainController mainController) {
        this.mainController = mainController;
        root = mainController.getEqStackPane();
    }

    void setEqBuildFSMController(EqBuildFSMController eqBuildFSMController) {
        this.eqBuildFSMController = eqBuildFSMController;
    }

    void setEqHomeController(EqHomeController eqHomeController) {
        this.eqHomeController = eqHomeController;
    }

    void setEqRunFSMController(EqRunFSMController eqRunFSMController) {
        this.eqRunFSMController = eqRunFSMController;
    }

    public void setEqExamplesController(EqExamplesController eqExamplesController) {
        this.eqExamplesController = eqExamplesController;
    }

    StackPane getRoot() {
        return root;
    }

    public void loadEqHome() {
        try {
            AnchorPane EqHome = FXMLLoader.load(getClass().getResource("/view/equivalence/EqHome.fxml"));
            root.getChildren().setAll(EqHome);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadEqExamples() {
        try {
            AnchorPane EqExamples = FXMLLoader.load(getClass().getResource("/view/equivalence/EqExamples.fxml"));
            root.getChildren().setAll(EqExamples);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void loadEqBuildFSM(FSM machine) {
        try {
            AnchorPane EqBuildFSM = FXMLLoader.load(getClass().getResource("/view/equivalence/EqBuildFSM.fxml"));
            root.getChildren().setAll(EqBuildFSM);
            eqBuildFSMController.loadFSM(machine);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void loadEqRunFSM(FSM machine, boolean direct) {
        try {
            AnchorPane EqRunFSM = FXMLLoader.load(getClass().getResource("/view/equivalence/EqRunFSM.fxml"));
            root.getChildren().setAll(EqRunFSM);
            eqRunFSMController.runEqFSMConstruction(machine, direct);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private EqControllerMediator() {}

    public static EqControllerMediator getInstance() {
        return ControllerMediatorHolder.INSTANCE;
    }

    private static class ControllerMediatorHolder {
        private static final EqControllerMediator INSTANCE = new EqControllerMediator();
    }
}
