package com.guimata.controller.equivalence;

import com.github.jabbalaci.graphviz.GraphViz;
import com.guimata.controller.common.BuildFSM;
import com.guimata.controller.computation.CControllerMediator;
import com.guimata.controller.normalForms.NFControllerMediator;
import com.jfoenix.controls.*;
import com.libfsm.automata.machines.DFA;
import com.libfsm.automata.machines.FSM;
import com.libfsm.automata.machines.FSMType;

import com.libfsm.automata.machines.NFA;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.stream.Collectors;

import static com.guimata.controller.common.BuildFSM.displayInfoDialog;


public class EqBuildFSMController implements Initializable {
    private GraphViz graphViz = new GraphViz();
    private FSM machine;

    @FXML
    private ScrollPane scrollPane;

    @FXML
    private JFXRadioButton stepRadioButton;

    @FXML
    private ToggleGroup toggleGroup;

    @FXML
    private JFXRadioButton directRadioButton;

    @FXML
    private Text alphabetText;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        EqControllerMediator.getInstance().setEqBuildFSMController(this);
    }

    @FXML
    void changeToEqHome(ActionEvent event) {
        EqControllerMediator.getInstance().loadEqHome();
    }

    @FXML
    void addState(ActionEvent event) {
        BuildFSM.addState(EqControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void removeState(ActionEvent event) {
        BuildFSM.removeState(EqControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void renameState(ActionEvent event) {
        BuildFSM.renameState(EqControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void addTransition(ActionEvent event) {
        BuildFSM.addTransition(EqControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void removeTransition(ActionEvent event) {
        BuildFSM.removeTransition(EqControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void defineAlphabet(ActionEvent event) {
        BuildFSM.defineAlphabet(EqControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void clearAutomaton(ActionEvent event) {
        if (machine.getFsmType().equals(FSMType.DFA))
            machine = new DFA("empty");
        else
            machine = new NFA("empty");
        BuildFSM.displayDiagramAndAlphabet(scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void setInitial(ActionEvent event) {
        BuildFSM.setInitial(EqControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void chooseModel(ActionEvent event) {
        if (!machine.isCorrectlyDefined()) {
            BuildFSM.displayInfoDialog(EqControllerMediator.getInstance().getRoot(), machine.notCorrectlyDefinedText());
            return;
        }

        JFXDialogLayout content = new JFXDialogLayout();
        VBox inputItems = new VBox(10);

        Set<String> models = new HashSet<>();

        models.add("DFA");

        ToggleGroup tg = new ToggleGroup();

        for (String normalForm : models) {
            JFXRadioButton formButton = new JFXRadioButton();
            formButton.setText(normalForm);
            tg.getToggles().add(formButton);
            inputItems.getChildren().add(formButton);
        }

        content.setBody(inputItems);

        JFXButton cancel = new JFXButton("Cancel");
        cancel.setButtonType(JFXButton.ButtonType.RAISED);
        JFXButton run = new JFXButton("Run");
        run.setButtonType(JFXButton.ButtonType.RAISED);
        content.setActions(cancel, run);

        JFXDialog dialog = new JFXDialog(EqControllerMediator.getInstance().getRoot(), content, JFXDialog.DialogTransition.CENTER);
        content.setPrefWidth(200);
        dialog.setPrefWidth(200);
        cancel.setOnAction(e -> dialog.close());
        run.setOnAction(e -> {
            if (tg.getSelectedToggle() == null) {
                BuildFSM.displayInfoDialog(EqControllerMediator.getInstance().getRoot(), "Select which model to construct!");
                dialog.close();
                return;
            }

            if (!machine.isCorrectlyDefined()) {
                displayInfoDialog(CControllerMediator.getInstance().getRoot(), machine.notCorrectlyDefinedText());
                dialog.close();
                return;
            }

            for (var state : machine.getStates()) {
                if (state.startsWith("<init")) {
                    BuildFSM.displayInfoDialog(NFControllerMediator.getInstance().getRoot(), "Names of states can't contain prefix \"<init\" !");
                    dialog.close();
                    return;
                } else if (state.startsWith("<final")) {
                    BuildFSM.displayInfoDialog(NFControllerMediator.getInstance().getRoot(), "Names of states can't contain prefix \"<final\" !");
                    dialog.close();
                    return;
                }
            }

            boolean directConstruction = directRadioButton.isSelected();
            EqControllerMediator.getInstance().loadEqRunFSM(machine, directConstruction);
            dialog.close();
        });
        dialog.show();
    }

    @FXML
    void exportToFile(ActionEvent event) {
        BuildFSM.exportToFile(machine);
    }

    @FXML
    void renameFSM(ActionEvent event) {
        BuildFSM.renameFSM(EqControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    void loadFSM(FSM machine) {
        this.machine = machine;
        BuildFSM.displayDiagramAndAlphabet(scrollPane, graphViz, machine, alphabetText);
        alphabetText.setText(String.join(",", machine.getAlphabet()
                .stream()
                .map(Object::toString)
                .collect(Collectors.toSet())));
    }
}
