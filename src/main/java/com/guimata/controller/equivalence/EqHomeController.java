package com.guimata.controller.equivalence;

import com.libfsm.automata.machines.FSM;
import com.libfsm.automata.machines.FSMType;
import com.libfsm.automata.machines.NFA;
import com.libfsm.examples.dfa.DFAEx01;
import com.libfsm.examples.nfa.NFAEx01;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.FileChooser;

import java.io.File;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class EqHomeController implements Initializable {
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        EqControllerMediator.getInstance().setEqHomeController(this);
    }

    @FXML
    void changeNFAToDFAView(ActionEvent event) {
        FSM machine = new NFA("empty");
        EqControllerMediator.getInstance().loadEqBuildFSM(machine);
    }

    @FXML
    void importFSM(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("JSON", "*.json"));
        Optional<File> selectedFile = Optional.ofNullable(fileChooser.showOpenDialog(null));

        if (selectedFile.isPresent() && selectedFile.get().getName().endsWith(".json")) {
            FSM machine = FSM.readFromFile(selectedFile.get());
            if (machine.getFsmType().equals(FSMType.NFA))
                EqControllerMediator.getInstance().loadEqBuildFSM(machine);
        }
    }

    @FXML
    void changeToExamplesView(ActionEvent event) {
        EqControllerMediator.getInstance().loadEqExamples();
    }
}
