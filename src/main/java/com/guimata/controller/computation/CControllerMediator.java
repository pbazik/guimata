package com.guimata.controller.computation;

import com.guimata.controller.common.MainController;
import com.libfsm.automata.machines.FSM;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;

import java.io.IOException;

public class CControllerMediator {
    private MainController mainController;
    private CHomeController cHomeController;
    private CBuildFSMController cBuildFSMController;
    private CRunFSMController cRunFSMController;
    private CExamplesController cExamplesController;

    @FXML
    private StackPane root;

    public void setMainController(MainController mainController) {
        this.mainController = mainController;
        root = mainController.getcStackPane();
    }

    public void setcHomeController(CHomeController cHomeController) {
        this.cHomeController = cHomeController;
    }

    public void setcBuildFSMController(CBuildFSMController cBuildFSMController) {
        this.cBuildFSMController = cBuildFSMController;
    }

    public void setcRunFSMController(CRunFSMController cRunFSMController) {
        this.cRunFSMController = cRunFSMController;
    }

    public void setcExamplesController(CExamplesController cExamplesController) {
        this.cExamplesController = cExamplesController;
    }

    public StackPane getRoot() {
        return root;
    }

    public void loadCHome() {
        try {
            AnchorPane computationHome = FXMLLoader.load(getClass().getResource("/view/computation/CHome.fxml"));
            root.getChildren().setAll(computationHome);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadCExamples() {
        try {
            AnchorPane computationExamples = FXMLLoader.load(getClass().getResource("/view/computation/CExamples.fxml"));
            root.getChildren().setAll(computationExamples);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadCBuildFSM(FSM machine) {
        try {
            AnchorPane CBuildFSM = FXMLLoader.load(getClass().getResource("/view/computation/CBuildFSM.fxml"));
            root.getChildren().setAll(CBuildFSM);
            cBuildFSMController.loadFSM(machine);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadCRunFSM(FSM machine, String word, boolean direct) {
        try {
            AnchorPane CRunFSM = FXMLLoader.load(getClass().getResource("/view/computation/CRunFSM.fxml"));
            root.getChildren().setAll(CRunFSM);
            cRunFSMController.startComputation(machine, word, direct);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private CControllerMediator() {}

    public static CControllerMediator getInstance() {
        return ControllerMediatorHolder.INSTANCE;
    }

    private static class ControllerMediatorHolder {
        private static final CControllerMediator INSTANCE = new CControllerMediator();
    }
}
