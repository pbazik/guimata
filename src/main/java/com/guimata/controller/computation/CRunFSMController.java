package com.guimata.controller.computation;

import com.github.jabbalaci.graphviz.GraphViz;
import com.jfoenix.controls.JFXButton;
import com.libfsm.automata.machines.FSM;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ScrollPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.stream.Collectors;

import static com.guimata.controller.common.BuildFSM.displayInfoDialog;
import static com.guimata.controller.common.BuildFSM.updateDiagram;

public class CRunFSMController implements Initializable {
    private GraphViz graphViz = new GraphViz();
    private FSM machine;
    private Set<String> currentStates;
    private String wordToCompute;
    private boolean directComputation = false;

    @FXML
    private ScrollPane scrollPane;

    @FXML
    private TextFlow textFlow;

    @FXML
    private JFXButton nextButton;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        CControllerMediator.getInstance().setcRunFSMController(this);
    }

    @FXML
    void changeToFSMBuild(ActionEvent event) {
        CControllerMediator.getInstance().loadCBuildFSM(machine);
    }

    @FXML
    void nextStep(ActionEvent event) {
        if (directComputation) {
            completeComputation();
            nextButton.setDisable(true);
            return;
        }

        currentStates = machine.computeStep(currentStates, wordToCompute.charAt(0));

        if (currentStates.isEmpty()) {
            displayInfoDialog(CControllerMediator.getInstance().getRoot(), "Halted");
            nextButton.setDisable(true);
            return;
        }

        wordToCompute = wordToCompute.substring(1);
        displayConfiguration(currentStates, wordToCompute);

        if  (wordToCompute.isEmpty()) {
            displayInfoDialog(CControllerMediator.getInstance().getRoot(), currentStates.stream().anyMatch(machine::isFinalState) ? "Accepted" : "Rejected");
            nextButton.setDisable(true);
        }
    }

    void startComputation(FSM machine, String word, boolean direct) {
        this.machine = machine;
        this.directComputation = direct;
        initComputation(word);
    }

    private void initComputation(String word) {
        currentStates = machine.epsilonClosure(Set.of(machine.getInitial()));
        wordToCompute = word;

        if (word.equals("$"))
            displayInfoDialog(CControllerMediator.getInstance().getRoot(), currentStates.stream().anyMatch(machine::isFinalState) ? "Accepted" : "Rejected");

        displayConfiguration(currentStates, word);
    }

    private void completeComputation() {
        var result = machine.compute(wordToCompute, false);
        currentStates = result.getFirst();

        if (currentStates.isEmpty()) {
            wordToCompute = wordToCompute.substring(result.getThird());
            displayInfoDialog(CControllerMediator.getInstance().getRoot(), "Halted");
            displayConfiguration(currentStates, wordToCompute);
            return;
        }

        displayInfoDialog(CControllerMediator.getInstance().getRoot(), currentStates.stream().anyMatch(machine::isFinalState) ? "Accepted" : "Rejected");

        wordToCompute = "";
        displayConfiguration(currentStates, wordToCompute);
    }

    private void displayConfiguration(Set<String> states, String word) {
        buildDiagramFromConfiguration(states, word);
        updateDiagram(graphViz, scrollPane);
        updateInputWordText(word);
    }

    private void buildDiagramFromConfiguration(Set<String> states, String word) {
        graphViz.clearGraph();
        graphViz.addln(graphViz.start_graph());
        graphViz.addln("rankdir=LR;");
        graphViz.addln("init [shape = none, label = \"\"];");

        machine.getStates().forEach(q -> {
            String colored = states.contains(q) ? "style = filled, fillcolor = \"#50A14F\", " : "";
            String typeState = machine.isFinalState(q) ? "shape = doublecircle];" : "shape = circle];";
            graphViz.addln("\""+q+"\"" + " [" + colored + typeState);
        });

        if (!machine.getInitial().isEmpty())
            graphViz.addln("init -> " + "\""+ machine.getInitial() +"\""+ ";");

        machine.getStates().forEach(p -> {
            machine.getStates().forEach(q -> {
                Set<String> chars = machine.getAllTransitionsFromTo(p, q)
                        .stream()
                        .map(Object::toString)
                        .collect(Collectors.toSet());

                if (!chars.isEmpty()) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("\"").append(p).append("\" -> \"").append(q).append("\"");

                    if (word.isEmpty()) {
                        sb.append(" [label = <").append(String.join(" , ", chars)).append(">];");
                    } else {
                        sb.append(" [label = <");
                        var charIt = chars.iterator();
                        boolean any = false;
                        String current = charIt.next();

                        if (states.contains(p) && (current.equals(String.valueOf(word.charAt(0))))) {
                            any = true;
                            sb.append("<font color=\"#50A14F\">").append(current).append("</font>");
                        } else {
                            sb.append(current);
                        }

                        while (charIt.hasNext()) {
                            current = charIt.next();
                            sb.append(" , ");
                            if (states.contains(p) && (current.equals(String.valueOf(word.charAt(0))))) {
                                any = true;
                                sb.append("<font color=\"#50A14F\">").append(current).append("</font>");
                            } else {
                                sb.append(current);
                            }
                        }
                        sb.append(">");

                        if (any)
                            sb.append(", color = \"#50A14F\"");

                        sb.append("];");

                    }
                    graphViz.addln(sb.toString());
                }
            });
        });

        graphViz.addln(graphViz.end_graph());
    }

    private void updateInputWordText(String word) {
        Text head = !word.isEmpty() ? new Text(""+word.charAt(0)) : new Text("");
        head.setStyle("-fx-font-family: Hack; -fx-font-size: 20;");
        head.setFill(Color.RED);

        Text tail = word.length() > 1 ? new Text(word.substring(1)) : new Text("");
        tail.setStyle("-fx-font-family: Hack; -fx-font-size: 20;");
        textFlow.setCenterShape(true);
        textFlow.getChildren().setAll(head, tail);
    }
}
