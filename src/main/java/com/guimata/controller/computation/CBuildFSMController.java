package com.guimata.controller.computation;

import com.github.jabbalaci.graphviz.GraphViz;
import com.guimata.controller.common.BuildFSM;
import com.jfoenix.controls.*;
import com.libfsm.automata.machines.DFA;
import com.libfsm.automata.machines.FSM;
import com.libfsm.automata.machines.FSMType;
import com.libfsm.automata.machines.NFA;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;

import java.io.File;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.stream.Collectors;

import static com.guimata.controller.common.BuildFSM.displayInfoDialog;

public class CBuildFSMController implements Initializable {
    private GraphViz graphViz = new GraphViz();
    private FSM machine;

    @FXML
    private ScrollPane scrollPane;

    @FXML
    private JFXTextField wordInput;

    @FXML
    private Text alphabetText;

    @FXML
    private JFXRadioButton stepRadioButton;

    @FXML
    private ToggleGroup toggleGroup;

    @FXML
    private JFXRadioButton directRadioButton;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        CControllerMediator.getInstance().setcBuildFSMController(this);
    }

    @FXML
    void changeToComputationHome(ActionEvent event) {
        CControllerMediator.getInstance().loadCHome();
    }

    @FXML
    void addState(ActionEvent event) {
        BuildFSM.addState(CControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void removeState(ActionEvent event) {
        BuildFSM.removeState(CControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void renameState(ActionEvent event) {
        BuildFSM.renameState(CControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void addTransition(ActionEvent event) {
        BuildFSM.addTransition(CControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void removeTransition(ActionEvent event) {
        BuildFSM.removeTransition(CControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void defineAlphabet(ActionEvent event) {
        BuildFSM.defineAlphabet(CControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void clearAutomaton(ActionEvent event) {
        if (machine.getFsmType().equals(FSMType.DFA))
            machine = new DFA("empty");
        else
            machine = new NFA("empty");
        BuildFSM.displayDiagramAndAlphabet(scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void setInitial(ActionEvent event) {
        BuildFSM.setInitial(CControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void runComputation(ActionEvent event) {
        String word = wordInput.getText();

        if (word.isEmpty()) {
            displayInfoDialog(CControllerMediator.getInstance().getRoot(), "Enter word to compute!\nFor empty word, use \'$\'.");
            return;
        }

        if (!machine.isCorrectlyDefined()) {
            displayInfoDialog(CControllerMediator.getInstance().getRoot(), machine.notCorrectlyDefinedText());
            return;
        }

        boolean directComputation = directRadioButton.isSelected();
        CControllerMediator.getInstance().loadCRunFSM(machine, word, directComputation);
    }

    @FXML
    void exportToFile(ActionEvent event) {
        BuildFSM.exportToFile(machine);
    }

    @FXML
    void renameFSM(ActionEvent event) {
        BuildFSM.renameFSM(CControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    void loadFSM(FSM machine) {
        this.machine = machine;
        BuildFSM.displayDiagramAndAlphabet(scrollPane, graphViz, machine, alphabetText);
        alphabetText.setText(String.join(",", machine.getAlphabet()
                .stream()
                .map(Object::toString)
                .collect(Collectors.toSet())));
    }
}
