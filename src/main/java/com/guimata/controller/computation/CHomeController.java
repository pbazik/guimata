package com.guimata.controller.computation;

import com.libfsm.automata.machines.DFA;
import com.libfsm.automata.machines.FSM;
import com.libfsm.automata.machines.NFA;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.FileChooser;

import java.io.*;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class CHomeController implements Initializable {

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        CControllerMediator.getInstance().setcHomeController(this);
    }

    @FXML
    void changeToDFAView(ActionEvent event) {
        FSM machine = new DFA("empty");
        CControllerMediator.getInstance().loadCBuildFSM(machine);
    }

    @FXML
    void changeToNFAView(ActionEvent event) {
        FSM machine = new NFA("empty");
        CControllerMediator.getInstance().loadCBuildFSM(machine);
    }

    @FXML
    void importFSM(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("JSON", "*.json"));
        Optional<File> selectedFile = Optional.ofNullable(fileChooser.showOpenDialog(null));

        if (selectedFile.isPresent() && selectedFile.get().getName().endsWith(".json")) {
            FSM machine = FSM.readFromFile(selectedFile.get());
            CControllerMediator.getInstance().loadCBuildFSM(machine);
        }
    }

    @FXML
    void changeToExamplesView(ActionEvent event) {
        CControllerMediator.getInstance().loadCExamples();
    }
}
