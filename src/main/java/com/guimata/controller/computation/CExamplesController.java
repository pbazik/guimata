package com.guimata.controller.computation;

import com.libfsm.automata.machines.FSM;
import com.libfsm.examples.dfa.DFAEx01;
import com.libfsm.examples.nfa.NFAEx01;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.FileChooser;

import java.io.File;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class CExamplesController implements Initializable {

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        CControllerMediator.getInstance().setcExamplesController(this);
    }

    @FXML
    void changeToHome(ActionEvent event) {
        CControllerMediator.getInstance().loadCHome();
    }

    @FXML
    void endsWithAButton(ActionEvent event) {
        FSM machine = NFAEx01.get();
        CControllerMediator.getInstance().loadCBuildFSM(machine);
    }

    @FXML
    void numberAsButton(ActionEvent event) {
        FSM machine = DFAEx01.get();
        CControllerMediator.getInstance().loadCBuildFSM(machine);
    }

}
