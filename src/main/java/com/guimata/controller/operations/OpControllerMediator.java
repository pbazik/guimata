package com.guimata.controller.operations;

import com.guimata.controller.common.MainController;
import com.libfsm.automata.machines.FSM;
import com.libfsm.automata.machines.FSMType;
import com.libfsm.operations.common.OperationType;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;

import java.io.IOException;

public class OpControllerMediator {
    private MainController mainController;
    private OpHomeController opHomeController;
    private OpChoiceFirstFSMController opChoiceFirstFSMController;
    private OpChoiceSecondFSMController opChoiceSecondFSMController;
    private OpBuildFirstFSMController opBuildFirstFSMController;
    private OpBuildSecondFSMController opBuildSecondFSMController;
    private OpRunUnaryFSMController opRunUnaryFSMController;
    private OpRunBinaryFSMController opRunBinaryFSMController;
    private OpExamplesController opExamplesController;

    private FSMType typeAllowed;
    private OperationType type;
    private boolean unaryOp;
    private FSM first = null;
    private FSM second = null;

    @FXML
    private StackPane root;

    public void setMainController(MainController mainController) {
        this.mainController = mainController;
        root = mainController.getOpStackPane();
    }

    public void setOpHomeController(OpHomeController opHomeController) {
        this.opHomeController = opHomeController;
    }

    void setOpChoiceFirstFSMController(OpChoiceFirstFSMController opChoiceFirstFSMController) {
        this.opChoiceFirstFSMController = opChoiceFirstFSMController;
    }

    void setOpBuildFirstFSMController(OpBuildFirstFSMController opBuildFirstFSMController) {
        this.opBuildFirstFSMController = opBuildFirstFSMController;
    }

    void setOpBuildSecondFSMController(OpBuildSecondFSMController opBuildSecondFSMController) {
        this.opBuildSecondFSMController = opBuildSecondFSMController;
    }

    void setOpRunUnaryFSMController(OpRunUnaryFSMController opRunUnaryFSMController) {
        this.opRunUnaryFSMController = opRunUnaryFSMController;
    }

    void setOpRunBinaryFSMController(OpRunBinaryFSMController opRunBinaryFSMController) {
        this.opRunBinaryFSMController = opRunBinaryFSMController;
    }

    void setOpChoiceSecondFSMController(OpChoiceSecondFSMController opChoiceSecondFSMController) {
        this.opChoiceSecondFSMController = opChoiceSecondFSMController;
    }

    void setOpExamplesController(OpExamplesController opExamplesController) {
        this.opExamplesController = opExamplesController;
    }

    public StackPane getRoot() {
        return root;
    }

    FSMType getTypeAllowed() {
        return typeAllowed;
    }

    OperationType getType() {
        return type;
    }

    boolean isUnaryOp() {
        return unaryOp;
    }

    FSM getFirst() {
        return first;
    }

    FSM getSecond() {
        return second;
    }

    public void loadOpHome() {
        try {
            first = null;
            second = null;
            AnchorPane OpHome = FXMLLoader.load(getClass().getResource("/view/operations/OpHome.fxml"));
            root.getChildren().setAll(OpHome);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void loadOpExamples(String which) {
        try {
            AnchorPane OpExamples = FXMLLoader.load(getClass().getResource("/view/operations/OpExamples.fxml"));
            root.getChildren().setAll(OpExamples);
            opExamplesController.load(which);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void loadOpChoiceFSM(OperationType type) {
        this.type = type;

        switch (type) {
            case Union:
            case Reverse:
            case Concatenation:
                typeAllowed = FSMType.NFA;
                break;
            case Complement:
            case Intersection:
                typeAllowed = FSMType.DFA;
                break;
        }

        switch (type) {
            case Complement:
            case Reverse:
                unaryOp = true;
                break;
            case Union:
            case Intersection:
            case Concatenation:
                unaryOp = false;
                break;
        }

        if (!unaryOp) {
            try {
                AnchorPane OpChoiceFirstFSM = FXMLLoader.load(getClass().getResource("/view/operations/OpChoiceFirstFSM.fxml"));
                root.getChildren().setAll(OpChoiceFirstFSM);
                opChoiceFirstFSMController.loadChoice(type);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                AnchorPane OpChoiceSecondFSM = FXMLLoader.load(getClass().getResource("/view/operations/OpChoiceSecondFSM.fxml"));
                root.getChildren().setAll(OpChoiceSecondFSM);
                opChoiceSecondFSMController.loadChoice(type);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    void loadOpChoiceFirstFSM() {
        try {
            first = null;
            second = null;
            AnchorPane OpChoiceFirstFSM = FXMLLoader.load(getClass().getResource("/view/operations/OpChoiceFirstFSM.fxml"));
            root.getChildren().setAll(OpChoiceFirstFSM);
            opChoiceFirstFSMController.loadChoice(type);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void loadOpChoiceSecondFSM() {
        try {
            second = null;
            AnchorPane OpChoiceFirstFSM = FXMLLoader.load(getClass().getResource("/view/operations/OpChoiceSecondFSM.fxml"));
            root.getChildren().setAll(OpChoiceFirstFSM);
            opChoiceFirstFSMController.loadChoice(type);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void loadOpChoiceSecondFromFirstFSM(FSM machine) {
        try {
            first = machine;
            AnchorPane OpChoiceFirstFSM = FXMLLoader.load(getClass().getResource("/view/operations/OpChoiceSecondFSM.fxml"));
            root.getChildren().setAll(OpChoiceFirstFSM);
            opChoiceSecondFSMController.loadChoice(type);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void loadOpChoiceSecondFromSecondFSM(FSM machine) {
        try {
            second = machine;
            AnchorPane OpChoiceFirstFSM = FXMLLoader.load(getClass().getResource("/view/operations/OpChoiceSecondFSM.fxml"));
            root.getChildren().setAll(OpChoiceFirstFSM);
            opChoiceSecondFSMController.loadChoice(type);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void loadOpBuildFirstFSM() {
        try {
            AnchorPane OpBuildFirstFSM = FXMLLoader.load(getClass().getResource("/view/operations/OpBuildFirstFSM.fxml"));
            root.getChildren().setAll(OpBuildFirstFSM);
            opBuildFirstFSMController.loadFirstFSM(first);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void loadOpBuildFirstFSM(FSM machine) {
        try {
            first = machine;
            AnchorPane OpBuildFirstFSM = FXMLLoader.load(getClass().getResource("/view/operations/OpBuildFirstFSM.fxml"));
            root.getChildren().setAll(OpBuildFirstFSM);
            opBuildFirstFSMController.loadFirstFSM(machine);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void loadOpBuildSecondFSM(FSM machine) {
        try {
            second = machine;
            AnchorPane OpBuildSecondFSM = FXMLLoader.load(getClass().getResource("/view/operations/OpBuildSecondFSM.fxml"));
            root.getChildren().setAll(OpBuildSecondFSM);
            opBuildSecondFSMController.loadSecondFSM(machine);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void loadOpBuildSecondFSM(FSM first, FSM second) {
        try {
            this.first = first;
            this.second = second;
            AnchorPane OpBuildSecondFSM = FXMLLoader.load(getClass().getResource("/view/operations/OpBuildSecondFSM.fxml"));
            root.getChildren().setAll(OpBuildSecondFSM);
            opBuildSecondFSMController.loadSecondFSM(second);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void loadOpRunUnaryFSM(FSM machine, boolean direct) {
        try {
            second = machine;
            AnchorPane OpRunUnary = FXMLLoader.load(getClass().getResource("/view/operations/OpRunUnaryFSM.fxml"));
            root.getChildren().setAll(OpRunUnary);
            opRunUnaryFSMController.runOperation(second, direct);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void loadOpRunBinaryFSM(FSM machine, boolean direct) {
        try {
            second = machine;
            AnchorPane OpRunBinary = FXMLLoader.load(getClass().getResource("/view/operations/OpRunBinaryFSM.fxml"));
            root.getChildren().setAll(OpRunBinary);
            opRunBinaryFSMController.runOperation(first, second, direct);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private OpControllerMediator() {}

    public static OpControllerMediator getInstance() {
        return ControllerMediatorHolder.INSTANCE;
    }

    private static class ControllerMediatorHolder {
        private static final OpControllerMediator INSTANCE = new OpControllerMediator();
    }
}
