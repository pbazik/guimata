package com.guimata.controller.operations;

import com.guimata.controller.common.BuildFSM;
import com.guimata.controller.computation.CControllerMediator;
import com.libfsm.automata.machines.FSM;
import com.libfsm.examples.dfa.DFAEx01;
import com.libfsm.examples.nfa.NFAEx01;
import com.libfsm.operations.common.OperationType;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import java.net.URL;
import java.util.ResourceBundle;

public class OpExamplesController implements Initializable {
    String which;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        OpControllerMediator.getInstance().setOpExamplesController(this);
    }

    @FXML
    void changeToHome(ActionEvent event) {
        switch (which) {
            case "second":
                OpControllerMediator.getInstance().loadOpChoiceSecondFSM();
                break;

            case "first":
                OpControllerMediator.getInstance().loadOpChoiceFirstFSM();
                break;
        }
    }

    @FXML
    void endsWithAButton(ActionEvent event) {
        FSM machine = NFAEx01.get();

        if (OpControllerMediator.getInstance().isUnaryOp() || OpControllerMediator.getInstance().getFirst() != null) {
            if (OpControllerMediator.getInstance().getType().equals(OperationType.Complement) || OpControllerMediator.getInstance().getType().equals(OperationType.Intersection)) {
                BuildFSM.displayInfoDialog(OpControllerMediator.getInstance().getRoot(), "Cannot run on NFA");
                return;
            }
            OpControllerMediator.getInstance().loadOpBuildSecondFSM(machine);
        } else {
            if (OpControllerMediator.getInstance().getType().equals(OperationType.Intersection)) {
                BuildFSM.displayInfoDialog(OpControllerMediator.getInstance().getRoot(), "Cannot run on NFA");
                return;
            }
            OpControllerMediator.getInstance().loadOpBuildFirstFSM(machine);
        }
    }

    @FXML
    void numberAsButton(ActionEvent event) {
        FSM machine = DFAEx01.get();

        if (OpControllerMediator.getInstance().isUnaryOp() || OpControllerMediator.getInstance().getFirst() != null) {
            OpControllerMediator.getInstance().loadOpBuildSecondFSM(machine);
        } else {
            OpControllerMediator.getInstance().loadOpBuildFirstFSM(machine);
        }
        //Petinka je super
    }

    void load(String which) {
        this.which = which;
    }
}
