package com.guimata.controller.operations;

import com.libfsm.operations.common.OperationType;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

public class OpHomeController {
    @FXML
    void changeComplementView(ActionEvent event) {
        OpControllerMediator.getInstance().loadOpChoiceFSM(OperationType.Complement);
    }

    @FXML
    void changeConcatenationView(ActionEvent event) {
        OpControllerMediator.getInstance().loadOpChoiceFSM(OperationType.Concatenation);
    }

    @FXML
    void changeIntersectionView(ActionEvent event) {
        OpControllerMediator.getInstance().loadOpChoiceFSM(OperationType.Intersection);
    }

    @FXML
    void changeReverseView(ActionEvent event) {
        OpControllerMediator.getInstance().loadOpChoiceFSM(OperationType.Reverse);
    }

    @FXML
    void changeUnionView(ActionEvent event) {
        OpControllerMediator.getInstance().loadOpChoiceFSM(OperationType.Union);
    }
}
