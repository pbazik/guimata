package com.guimata.controller.operations;

import com.guimata.controller.common.BuildFSM;
import com.jfoenix.controls.JFXButton;
import com.libfsm.automata.machines.DFA;
import com.libfsm.automata.machines.FSM;
import com.libfsm.automata.machines.FSMType;
import com.libfsm.automata.machines.NFA;
import com.libfsm.operations.common.OperationType;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.FileChooser;

import java.io.File;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class OpChoiceSecondFSMController implements Initializable {

    @FXML
    private JFXButton nfaButton;

    @FXML
    private JFXButton dfaButton;

    @FXML
    private JFXButton backButton;

    @FXML
    private JFXButton nextButton;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        OpControllerMediator.getInstance().setOpChoiceSecondFSMController(this);
        if (OpControllerMediator.getInstance().getSecond() == null)
            nextButton.setDisable(true);
    }

    @FXML
    void changeToOpBuildFirstFSM(ActionEvent event) {
        if (OpControllerMediator.getInstance().isUnaryOp())
            OpControllerMediator.getInstance().loadOpHome();
        else
            OpControllerMediator.getInstance().loadOpBuildFirstFSM();
    }

    @FXML
    void changeToOpBuildSecondFSM(ActionEvent event) {
        OpControllerMediator.getInstance().loadOpBuildSecondFSM(
                OpControllerMediator.getInstance().getSecond()
        );
    }

    @FXML
    void changeToDFAView(ActionEvent event) {
        FSM machine = new DFA("empty");
        OpControllerMediator.getInstance().loadOpBuildSecondFSM(machine);
    }

    @FXML
    void changeToNFAView(ActionEvent event) {
        FSM machine = new NFA("empty");
        OpControllerMediator.getInstance().loadOpBuildSecondFSM(machine);
    }

    @FXML
    void importFSM(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("JSON", "*.json"));
        Optional<File> selectedFile = Optional.ofNullable(fileChooser.showOpenDialog(null));

        if (selectedFile.isPresent() && selectedFile.get().getName().endsWith(".json")) {
            FSM machine = FSM.readFromFile(selectedFile.get());

            if (machine.getFsmType().equals(FSMType.NFA) && OpControllerMediator.getInstance().getTypeAllowed().equals(FSMType.DFA)) {
                BuildFSM.displayInfoDialog(OpControllerMediator.getInstance().getRoot(), "NFA is not allowed here!");
                return;
            }

            OpControllerMediator.getInstance().loadOpBuildSecondFSM(machine);
        }
    }

    @FXML
    void changeToExamplesView(ActionEvent event) {
        OpControllerMediator.getInstance().loadOpExamples("second");
    }

    void loadChoice(OperationType type) {
        switch (type) {
            case Complement:
            case Intersection:
                nfaButton.setVisible(false);
                break;
        }

    }

}
