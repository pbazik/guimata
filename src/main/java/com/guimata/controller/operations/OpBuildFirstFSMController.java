package com.guimata.controller.operations;

import com.github.jabbalaci.graphviz.GraphViz;
import com.guimata.controller.common.BuildFSM;
import com.guimata.controller.computation.CControllerMediator;
import com.libfsm.automata.machines.DFA;
import com.libfsm.automata.machines.FSM;
import com.libfsm.automata.machines.FSMType;
import com.libfsm.automata.machines.NFA;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ScrollPane;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class OpBuildFirstFSMController implements Initializable {
    private GraphViz graphViz = new GraphViz();
    private FSM machine;

    @FXML
    private ScrollPane scrollPane;

    @FXML
    private Text alphabetText;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        OpControllerMediator.getInstance().setOpBuildFirstFSMController(this);
    }

    @FXML
    void changeToOpChoiceFirstFSM(ActionEvent event) {
        OpControllerMediator.getInstance().loadOpChoiceFirstFSM();
    }

    @FXML
    void changeToOpChoiceSecondFSM(ActionEvent event) {
        OpControllerMediator.getInstance().loadOpChoiceSecondFromFirstFSM(machine);
    }

    @FXML
    void addState(ActionEvent event) {
        BuildFSM.addState(OpControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void removeState(ActionEvent event) {
        BuildFSM.removeState(OpControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void renameState(ActionEvent event) {
        BuildFSM.renameState(OpControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void addTransition(ActionEvent event) {
        BuildFSM.addTransition(OpControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void removeTransition(ActionEvent event) {
        BuildFSM.removeTransition(OpControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void defineAlphabet(ActionEvent event) {
        BuildFSM.defineAlphabet(OpControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void setInitial(ActionEvent event) {
        BuildFSM.setInitial(OpControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void clearAutomaton(ActionEvent event) {
        if (machine.getFsmType().equals(FSMType.DFA))
            machine = new DFA("empty");
        else
            machine = new NFA("empty");
        BuildFSM.displayDiagramAndAlphabet(scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void exportToFile(ActionEvent event) {
        BuildFSM.exportToFile(machine);
    }

    @FXML
    void renameFSM(ActionEvent event) {
        BuildFSM.renameFSM(OpControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    void loadFirstFSM(FSM machine) {
        this.machine = machine;
        BuildFSM.displayDiagramAndAlphabet(scrollPane, graphViz, machine, alphabetText);
        alphabetText.setText(String.join(",", machine.getAlphabet()
                .stream()
                .map(Object::toString)
                .collect(Collectors.toSet())));
    }
}
