package com.guimata.controller.operations;

import com.github.jabbalaci.graphviz.GraphViz;
import com.guimata.controller.common.BuildFSM;
import com.guimata.controller.computation.CControllerMediator;
import com.jfoenix.controls.JFXRadioButton;
import com.libfsm.automata.helpers.Sets;
import com.libfsm.automata.machines.DFA;
import com.libfsm.automata.machines.FSM;
import com.libfsm.automata.machines.FSMType;
import com.libfsm.automata.machines.NFA;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToggleGroup;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import static com.guimata.controller.common.BuildFSM.displayInfoDialog;

public class OpBuildSecondFSMController implements Initializable {
    private GraphViz graphViz = new GraphViz();
    private FSM machine;

    @FXML
    private ScrollPane scrollPane;

    @FXML
    private JFXRadioButton stepRadioButton;

    @FXML
    private ToggleGroup toggleGroup;

    @FXML
    private JFXRadioButton directRadioButton;

    @FXML
    private Text alphabetText;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        OpControllerMediator.getInstance().setOpBuildSecondFSMController(this);
    }

    @FXML
    void changeToOpChoiceSecondView(ActionEvent event) {
        OpControllerMediator.getInstance().loadOpChoiceSecondFromSecondFSM(machine);
    }

    @FXML
    void addState(ActionEvent event) {
        BuildFSM.addState(OpControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void removeState(ActionEvent event) {
        BuildFSM.removeState(OpControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void renameState(ActionEvent event) {
        BuildFSM.renameState(OpControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void addTransition(ActionEvent event) {
        BuildFSM.addTransition(OpControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void removeTransition(ActionEvent event) {
        BuildFSM.removeTransition(OpControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void defineAlphabet(ActionEvent event) {
        BuildFSM.defineAlphabet(OpControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void setInitial(ActionEvent event) {
        BuildFSM.setInitial(OpControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void clearAutomaton(ActionEvent event) {
        if (machine.getFsmType().equals(FSMType.DFA))
            machine = new DFA("empty");
        else
            machine = new NFA("empty");
        BuildFSM.displayDiagramAndAlphabet(scrollPane, graphViz, machine, alphabetText);
    }

    @FXML
    void exportToFile(ActionEvent event) {
        BuildFSM.exportToFile(machine);
    }

    @FXML
    void runOperation(ActionEvent event) {
        boolean unary = OpControllerMediator.getInstance().isUnaryOp();
        boolean direct = directRadioButton.isSelected();

        var second = OpControllerMediator.getInstance().getSecond();

        if (!unary) {
            var first = OpControllerMediator.getInstance().getFirst();

            for (var state : first.getStates()) {
                if (state.startsWith("<init")) {
                    BuildFSM.displayInfoDialog(OpControllerMediator.getInstance().getRoot(), "Names of states can't contain prefix \"<init\" !");
                    return;
                } else if (state.startsWith("<final")) {
                    BuildFSM.displayInfoDialog(OpControllerMediator.getInstance().getRoot(), "Names of states can't contain prefix \"<final\" !");
                    return;
                }
            }

            if (!machine.isCorrectlyDefined()) {
                displayInfoDialog(CControllerMediator.getInstance().getRoot(), first.notCorrectlyDefinedText());
                return;
            }

            if (!Sets.intersection(first.getStates(), second.getStates()).isEmpty()) {
                BuildFSM.displayInfoDialog(OpControllerMediator.getInstance().getRoot(), "Names of states have to be disjoint!");
                return;
            }

            if (first.getName().equals(second.getName())) {
                BuildFSM.displayInfoDialog(OpControllerMediator.getInstance().getRoot(), "Names of input automata have to be different!");
                return;
            }
        }

        for (var state : second.getStates()) {
            if (state.startsWith("<init")) {
                BuildFSM.displayInfoDialog(OpControllerMediator.getInstance().getRoot(), "Names of states can't contain prefix \"<init\" !");
                return;
            } else if (state.startsWith("<final")) {
                BuildFSM.displayInfoDialog(OpControllerMediator.getInstance().getRoot(), "Names of states can't contain prefix \"<final\" !");
                return;
            }
        }

        if (!machine.isCorrectlyDefined()) {
            displayInfoDialog(CControllerMediator.getInstance().getRoot(), second.notCorrectlyDefinedText());
            return;
        }


        if (unary)
            OpControllerMediator.getInstance().loadOpRunUnaryFSM(machine, direct);
        else
            OpControllerMediator.getInstance().loadOpRunBinaryFSM(machine, direct);
    }

    @FXML
    void renameFSM(ActionEvent event) {
        BuildFSM.renameFSM(OpControllerMediator.getInstance().getRoot(), scrollPane, graphViz, machine, alphabetText);
    }

    void loadSecondFSM(FSM machine) {
        this.machine = machine;
        BuildFSM.displayDiagramAndAlphabet(scrollPane, graphViz, machine, alphabetText);
        alphabetText.setText(String.join(",", machine.getAlphabet()
                .stream()
                .map(Object::toString)
                .collect(Collectors.toSet())));
    }
}
