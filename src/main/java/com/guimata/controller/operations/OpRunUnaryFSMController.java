package com.guimata.controller.operations;

import com.github.jabbalaci.graphviz.GraphViz;
import com.guimata.controller.common.BuildFSM;
import com.guimata.controller.common.RunFSM;
import com.jfoenix.controls.JFXButton;
import com.libfsm.automata.helpers.Sets;
import com.libfsm.automata.helpers.Triple;
import com.libfsm.automata.machines.FSM;
import com.libfsm.automata.machines.NFA;
import com.libfsm.normalforms.common.NormalFormFactory;
import com.libfsm.normalforms.common.NormalFormType;
import com.libfsm.operations.unary.UnaryOperation;
import com.libfsm.operations.unary.UnaryOperationFactory;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.*;

public class OpRunUnaryFSMController implements Initializable {
    private GraphViz graphViz = new GraphViz();
    private UnaryOperation operation;
    private FSM machine;
    private FSM resultMachine;
    private FSM draw;
    private boolean direct = false;
    private Map<Integer, String> steps = new HashMap<>();
    private int algorithmLength;
    private int step;
    private Iterator iterator;

    @FXML
    private ScrollPane descriptionScrollPane;

    @FXML
    private Text descriptionText;

    @FXML
    private VBox leftVbox;

    @FXML
    private ScrollPane leftScrollPane;

    @FXML
    private Text leftAlphabet;

    @FXML
    private VBox rightVbox;

    @FXML
    private ScrollPane rightScrollPane;

    @FXML
    private Text rightAlphabet;

    @FXML
    private JFXButton nextButton;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        OpControllerMediator.getInstance().setOpRunUnaryFSMController(this);
        HBox.setHgrow(leftVbox, Priority.ALWAYS);
        HBox.setHgrow(rightVbox, Priority.ALWAYS);
        VBox.setVgrow(leftScrollPane, Priority.ALWAYS);
        VBox.setVgrow(rightScrollPane, Priority.ALWAYS);
        descriptionScrollPane.setVvalue(1.0);
    }

    @FXML
    void changeToOpBuildSecondFSM(ActionEvent event) {
        OpControllerMediator.getInstance().loadOpBuildSecondFSM(machine);
    }

    @FXML
    void nextStep(ActionEvent event) {
        if (direct) {
            machine = resultMachine;
            BuildFSM.displayInfoDialog(OpControllerMediator.getInstance().getRoot(), "Completed");
            BuildFSM.displayDiagramAndAlphabet(rightScrollPane, graphViz, resultMachine, rightAlphabet);
            nextButton.setDisable(true);
            return;
        }

        if (step >= algorithmLength) {
            machine = resultMachine;
            BuildFSM.displayInfoDialog(OpControllerMediator.getInstance().getRoot(), "Completed");
            BuildFSM.displayDiagramAndAlphabet(rightScrollPane, graphViz, draw, rightAlphabet);
            nextButton.setDisable(true);
            return;
        }

        if (!iterator.hasNext()) {
            step++;

            switch (OpControllerMediator.getInstance().getType()) {
                case Complement:
                    switch (step) {
                        case 1:
                            iterator = machine.getAlphabet().iterator();
                            break;

                        case 2:
                            iterator = machine.getStates().iterator();
                            break;

                        case 3:
                            var complementTransitions = new HashSet<Triple<String, Character, String>>();
                            machine.getStates().forEach(p ->
                                    machine.getStates().forEach(q ->
                                            machine.getAllTransitionsFromTo(p, q).forEach(c ->
                                                    complementTransitions.add(new Triple<>(p, c, q))
                                            )
                                    )
                            );
                            iterator = complementTransitions.iterator();
                            break;

                        case 4:
                            iterator = Set.of(machine.getInitial()).iterator();
                            break;

                        case 5:
                            iterator = Sets.difference(machine.getStates(), machine.getFinalStates()).iterator();
                            break;
                    }
                    break;


                case Reverse:
                    switch (step) {
                        case 1:
                            iterator = machine.getAlphabet().iterator();
                            break;

                        case 2:
                            iterator = machine.getStates().iterator();
                            break;

                        case 3:
                            var reverseTransitions = new HashSet<Triple<String, Character, String>>();
                            machine.getStates().forEach(p ->
                                    machine.getStates().forEach(q ->
                                            machine.getAllTransitionsFromTo(p, q).forEach(c ->
                                                    reverseTransitions.add(new Triple<>(q, c, p))
                                            )
                                    )
                            );
                            iterator = reverseTransitions.iterator();
                            break;

                        case 4:
                            iterator = machine.getFinalStates().iterator();
                            break;

                        case 5:
                            iterator = Set.of(machine.getInitial()).iterator();
                            break;
                    }
                    break;
            }


            BuildFSM.displayDiagramAndAlphabet(leftScrollPane, graphViz, machine, leftAlphabet);
            BuildFSM.displayDiagramAndAlphabet(rightScrollPane, graphViz, draw, rightAlphabet);

            descriptionText.setText(descriptionText.getText().concat(steps.get(step)).concat("\n"));
            descriptionScrollPane.setVvalue(1.0);
            return;
        }

        switch (OpControllerMediator.getInstance().getType()) {
            case Complement:
                switch (step) {
                    case 0:
                        iterator.next();
                        break;

                    case 1:
                        var symbol = (Character)iterator.next();
                        draw.setAlphabet(Sets.union(draw.getAlphabet(), Set.of(symbol)));
                        BuildFSM.displayDiagramAndAlphabet(rightScrollPane, graphViz, draw, rightAlphabet);
                        descriptionText.setText(descriptionText.getText()
                                .concat("Added symbol \'")
                                .concat(symbol.toString())
                                .concat("\' to alphabet")
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);
                        break;

                    case 2:
                        var complementState = (String)iterator.next();
                        draw.addState(complementState);

                        RunFSM.displayStatesColored(leftScrollPane, graphViz, machine, Set.of(complementState), leftAlphabet);
                        RunFSM.displayStatesColored(rightScrollPane, graphViz, draw, Set.of(complementState), rightAlphabet);
                        descriptionText.setText(descriptionText.getText()
                                .concat("Added state ")
                                .concat(complementState)
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);
                        break;

                    case 3:
                        var complementTransition = (Triple<String, Character, String>)iterator.next();

                        draw.addTransition(complementTransition.getFirst(), complementTransition.getSecond(), complementTransition.getThird());

                        descriptionText.setText(descriptionText.getText()
                                .concat("Added transition from: ")
                                .concat(complementTransition.getFirst())
                                .concat(" on \'")
                                .concat(complementTransition.getSecond().toString())
                                .concat("\' to: ")
                                .concat(complementTransition.getThird())
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);

                        RunFSM.displayTransitionsColored(leftScrollPane, graphViz, machine, Set.of(complementTransition), leftAlphabet);
                        RunFSM.displayTransitionsColored(rightScrollPane, graphViz, draw, Set.of(complementTransition), rightAlphabet);
                        break;

                    case 4:
                        var complementInitial = (String)iterator.next();
                        draw.setInitial(complementInitial);

                        descriptionText.setText(descriptionText.getText()
                                .concat("Set initial state to: ")
                                .concat(draw.getInitial()).concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);

                        RunFSM.displayStatesColored(leftScrollPane, graphViz, machine, Set.of(), leftAlphabet);
                        RunFSM.displayStatesColored(rightScrollPane, graphViz, draw, Set.of(draw.getInitial()), rightAlphabet);
                        break;

                    case 5:
                        var complementFinalState = (String)iterator.next();
                        draw.addStateFinal(complementFinalState);

                        descriptionText.setText(descriptionText.getText()
                                .concat("Marked state ")
                                .concat(complementFinalState)
                                .concat(" as final")
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);

                        RunFSM.displayStatesColored(leftScrollPane, graphViz, machine, Set.of(complementFinalState), leftAlphabet);
                        RunFSM.displayStatesColored(rightScrollPane, graphViz, draw, Set.of(complementFinalState), rightAlphabet);
                        break;
                }
                break;


            case Reverse:
                switch (step) {
                    case 0:
                        iterator.next();
                        machine = new NormalFormFactory().get(NormalFormType.Piggy).apply(machine);
                        BuildFSM.displayDiagramAndAlphabet(leftScrollPane, graphViz, machine, leftAlphabet);
                        descriptionText.setText(descriptionText.getText().concat("Constructed nfa to its epsilon-free normal form").concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);
                        break;

                    case 1:
                        var symbol = (Character)iterator.next();
                        draw.setAlphabet(Sets.union(draw.getAlphabet(), Set.of(symbol)));
                        BuildFSM.displayDiagramAndAlphabet(rightScrollPane, graphViz, draw, rightAlphabet);
                        descriptionText.setText(descriptionText.getText()
                                .concat("Added symbol \'")
                                .concat(symbol.toString())
                                .concat("\' to alphabet")
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);
                        break;

                    case 2:
                        var reverseState = (String)iterator.next();
                        draw.addState(reverseState);

                        RunFSM.displayStatesColored(leftScrollPane, graphViz, machine, Set.of(reverseState), leftAlphabet);
                        RunFSM.displayStatesColored(rightScrollPane, graphViz, draw, Set.of(reverseState), rightAlphabet);
                        descriptionText.setText(descriptionText.getText()
                                .concat("Added state ")
                                .concat(reverseState)
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);
                        break;

                    case 3:
                        var reverseTransition = (Triple<String, Character, String>)iterator.next();

                        draw.addTransition(reverseTransition.getFirst(), reverseTransition.getSecond(), reverseTransition.getThird());

                        descriptionText.setText(descriptionText.getText()
                                .concat("Added transition from: ")
                                .concat(reverseTransition.getFirst())
                                .concat(" on \'")
                                .concat(reverseTransition.getSecond().toString())
                                .concat("\' to: ")
                                .concat(reverseTransition.getThird())
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);

                        RunFSM.displayTransitionsColored(leftScrollPane, graphViz, machine,
                                Set.of(new Triple<>(reverseTransition.getThird(),
                                        reverseTransition.getSecond(),
                                        reverseTransition.getFirst())), leftAlphabet);
                        RunFSM.displayTransitionsColored(rightScrollPane, graphViz, draw, Set.of(reverseTransition), rightAlphabet);
                        break;

                    case 4:
                        var reverseInitial = (String)iterator.next();
                        draw.setInitial(reverseInitial);

                        descriptionText.setText(descriptionText.getText()
                                .concat("Set initial state to: ")
                                .concat(draw.getInitial()).concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);

                        RunFSM.displayStatesColored(leftScrollPane, graphViz, machine, Set.of(draw.getInitial()), leftAlphabet);
                        RunFSM.displayStatesColored(rightScrollPane, graphViz, draw, Set.of(draw.getInitial()), rightAlphabet);
                        break;

                    case 5:
                        var reverseFinalState = (String)iterator.next();
                        draw.addStateFinal(reverseFinalState);

                        descriptionText.setText(descriptionText.getText()
                                .concat("Marked state ")
                                .concat(reverseFinalState)
                                .concat(" as final")
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);

                        RunFSM.displayStatesColored(leftScrollPane, graphViz, machine, Set.of(reverseFinalState), leftAlphabet);
                        RunFSM.displayStatesColored(rightScrollPane, graphViz, draw, Set.of(reverseFinalState), rightAlphabet);
                        break;
                }
                break;

        }
    }

    void runOperation(FSM machine, boolean direct) {
        this.machine = machine.copy();
        this.direct = direct;
        operation = new UnaryOperationFactory().get(OpControllerMediator.getInstance().getType());
        steps = operation.getSteps();
        resultMachine = operation.apply(machine);
        draw = new NFA("draw");
        algorithmLength = steps.size() - 1;
        step = 0;
        iterator = Set.of("convert").iterator();
        descriptionText.setText(steps.get(step).concat("\n"));
        descriptionScrollPane.setVvalue(1.0);
        BuildFSM.displayDiagramAndAlphabet(leftScrollPane, graphViz, machine, leftAlphabet);
    }
}
