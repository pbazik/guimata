package com.guimata.controller.operations;

import com.github.jabbalaci.graphviz.GraphViz;
import com.guimata.controller.common.BuildFSM;
import com.guimata.controller.common.RunFSM;
import com.guimata.controller.normalForms.NFControllerMediator;
import com.jfoenix.controls.JFXButton;
import com.libfsm.automata.helpers.Pair;
import com.libfsm.automata.helpers.Sets;
import com.libfsm.automata.helpers.Triple;
import com.libfsm.automata.machines.DFA;
import com.libfsm.automata.machines.FSM;
import com.libfsm.automata.machines.NFA;
import com.libfsm.normalforms.common.NormalFormFactory;
import com.libfsm.normalforms.common.NormalFormType;
import com.libfsm.operations.binary.BinaryOperation;
import com.libfsm.operations.common.OperationType;
import com.libfsm.operations.binary.BinaryOperationFactory;
import com.libfsm.operations.unary.UnaryOperationFactory;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.*;

public class OpRunBinaryFSMController implements Initializable {
    private GraphViz graphViz = new GraphViz();
    private BinaryOperation operation;
    private FSM firstMachine;
    private FSM secondMachine;
    private FSM resultMachine;
    private FSM draw;
    private boolean direct = false;
    private Map<Integer, String> steps = new HashMap<>();
    private int algorithmLength;
    private int step;
    private Iterator iterator;

    @FXML
    private VBox mainVbox;

    @FXML
    private HBox hbox;

    @FXML
    private VBox leftVbox;

    @FXML
    private ScrollPane leftScrollPane;

    @FXML
    private Text leftAlphabet;

    @FXML
    private VBox rightVbox;

    @FXML
    private ScrollPane rightScrollPane;

    @FXML
    private Text rightAlphabet;

    @FXML
    private VBox bottomVbox;

    @FXML
    private ScrollPane bottomScrollPane;

    @FXML
    private Text bottomAlphabet;

    @FXML
    private JFXButton nextButton;

    @FXML
    private ScrollPane descriptionScrollPane;

    @FXML
    private Text descriptionText;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        OpControllerMediator.getInstance().setOpRunBinaryFSMController(this);
        HBox.setHgrow(leftVbox, Priority.ALWAYS);
        HBox.setHgrow(rightVbox, Priority.ALWAYS);
        HBox.setHgrow(bottomVbox, Priority.ALWAYS);
        VBox.setVgrow(hbox, Priority.ALWAYS);
        VBox.setVgrow(bottomVbox, Priority.ALWAYS);
        VBox.setVgrow(leftScrollPane, Priority.ALWAYS);
        VBox.setVgrow(rightScrollPane, Priority.ALWAYS);
        VBox.setVgrow(bottomScrollPane, Priority.ALWAYS);
    }

    @FXML
    void changeToOpBuildSecondFSM(ActionEvent event) {
        OpControllerMediator.getInstance().loadOpBuildSecondFSM(firstMachine, secondMachine);
    }

    @FXML
    void nextStep(ActionEvent event) {
        if (direct) {
            secondMachine = resultMachine;
            BuildFSM.displayInfoDialog(OpControllerMediator.getInstance().getRoot(), "Completed");
            BuildFSM.displayDiagramAndAlphabet(bottomScrollPane, graphViz, resultMachine, bottomAlphabet);
            nextButton.setDisable(true);
            return;
        }

        if (step >= algorithmLength) {
            secondMachine = resultMachine;
            BuildFSM.displayInfoDialog(OpControllerMediator.getInstance().getRoot(), "Completed");
            BuildFSM.displayDiagramAndAlphabet(bottomScrollPane, graphViz, resultMachine, bottomAlphabet);
            nextButton.setDisable(true);
            return;
        }

        if (!iterator.hasNext()) {
            step++;

            switch (OpControllerMediator.getInstance().getType()) {
                case Intersection:
                    switch (step) {
                        case 1:
                            iterator = resultMachine.getAlphabet().iterator();
                            break;

                        case 2:
                            iterator = resultMachine.getStates().iterator();
                            break;

                        case 3:
                            var intersectionTransitions = new HashSet<Triple<String, Character, String>>();
                            resultMachine.getTransitions().forEach((k, v) -> {
                                v.forEach(q ->
                                    intersectionTransitions.add(new Triple<>(k.getFirst(), k.getSecond(), q))
                                );
                            });
                            iterator = intersectionTransitions.iterator();
                            break;

                        case 4:
                            iterator = Set.of(resultMachine.getInitial()).iterator();
                            break;

                        case 5:
                            iterator = resultMachine.getFinalStates().iterator();
                            break;
                    }
                    break;


                case Union:
                    switch (step) {
                        case 1:
                            iterator = resultMachine.getAlphabet().iterator();
                            break;

                        case 2:
                            iterator = firstMachine.getStates().iterator();
                            break;

                        case 3:
                            iterator = secondMachine.getStates().iterator();
                            break;

                        case 4:
                            iterator = Set.of(resultMachine.getInitial()).iterator();
                            break;

                        case 5:
                            var transitionsFirst = new HashSet<Triple<String, Character, String>>();
                            firstMachine.getTransitions().forEach((k, v) -> {
                                v.forEach(q ->
                                        transitionsFirst.add(new Triple<>(k.getFirst(), k.getSecond(), q))
                                );
                            });
                            iterator = transitionsFirst.iterator();
                            break;

                        case 6:
                            var transitionsSecond = new HashSet<Triple<String, Character, String>>();
                            secondMachine.getTransitions().forEach((k, v) -> {
                                v.forEach(q ->
                                        transitionsSecond.add(new Triple<>(k.getFirst(), k.getSecond(), q))
                                );
                            });
                            iterator = transitionsSecond.iterator();
                            break;

                        case 7:
                            var transitionsNew = new HashSet<Triple<String, Character, String>>();
                            transitionsNew.add(new Triple<>(resultMachine.getInitial(), FSM.EPSILON, firstMachine.getInitial()));
                            transitionsNew.add(new Triple<>(resultMachine.getInitial(), FSM.EPSILON, secondMachine.getInitial()));
                            iterator = transitionsNew.iterator();
                            break;

                        case 8:
                            iterator = Set.of(resultMachine.getInitial()).iterator();
                            break;

                        case 9:
                            iterator = firstMachine.getFinalStates().iterator();
                            break;

                        case 10:
                            iterator = secondMachine.getFinalStates().iterator();
                            break;
                    }
                    break;


                case Concatenation:
                    switch (step) {
                        case 1:
                            iterator = resultMachine.getAlphabet().iterator();
                            break;

                        case 2:
                            iterator = firstMachine.getStates().iterator();
                            break;

                        case 3:
                            iterator = secondMachine.getStates().iterator();
                            break;

                        case 4:
                            var transitionsFirst = new HashSet<Triple<String, Character, String>>();
                            firstMachine.getTransitions().forEach((k, v) -> {
                                v.forEach(q ->
                                        transitionsFirst.add(new Triple<>(k.getFirst(), k.getSecond(), q))
                                );
                            });
                            iterator = transitionsFirst.iterator();
                            break;

                        case 5:
                            var transitionsSecond = new HashSet<Triple<String, Character, String>>();
                            secondMachine.getTransitions().forEach((k, v) -> {
                                v.forEach(q ->
                                        transitionsSecond.add(new Triple<>(k.getFirst(), k.getSecond(), q))
                                );
                            });
                            iterator = transitionsSecond.iterator();
                            break;

                        case 6:
                            var firstFinal = firstMachine.getFinalStates().iterator().next();
                            var secondInitial = secondMachine.getInitial();
                            iterator = Set.of(new Triple<>(firstFinal, FSM.EPSILON, secondInitial)).iterator();
                            break;

                        case 7:
                            iterator = Set.of(resultMachine.getInitial()).iterator();
                            break;

                        case 8:
                            iterator = secondMachine.getFinalStates().iterator();
                            break;
                    }
                    break;
            }


            BuildFSM.displayDiagramAndAlphabet(leftScrollPane, graphViz, firstMachine, leftAlphabet);
            BuildFSM.displayDiagramAndAlphabet(rightScrollPane, graphViz, secondMachine, rightAlphabet);
            BuildFSM.displayDiagramAndAlphabet(bottomScrollPane, graphViz, draw, bottomAlphabet);

            descriptionText.setText(descriptionText.getText().concat(steps.get(step)).concat("\n"));
            descriptionScrollPane.setVvalue(1.0);
            return;
        }

        switch (OpControllerMediator.getInstance().getType()) {
            case Intersection:
                switch (step) {
                    case 0:
                        iterator.next();
                        break;

                    case 1:
                        var symbol = (Character)iterator.next();
                        draw.setAlphabet(Sets.union(draw.getAlphabet(), Set.of(symbol)));

                        descriptionText.setText(descriptionText.getText()
                                .concat("Added symbol \'")
                                .concat(symbol.toString())
                                .concat("\' to alphabet")
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);

                        BuildFSM.displayDiagramAndAlphabet(
                                bottomScrollPane,
                                graphViz,
                                draw,
                                bottomAlphabet
                        );
                        break;

                    case 2:
                        var intersectionState = (String)iterator.next();
                        draw.addState(intersectionState);

                        var pair = RunFSM.pairFromState(intersectionState);

                        descriptionText.setText(descriptionText.getText()
                                .concat("Added state ")
                                .concat(intersectionState)
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);

                        RunFSM.displayStatesColored(
                                leftScrollPane,
                                graphViz,
                                firstMachine,
                                Set.of(pair.getFirst()),
                                leftAlphabet
                        );

                        RunFSM.displayStatesColored(
                                rightScrollPane,
                                graphViz,
                                secondMachine,
                                Set.of(pair.getSecond()),
                                rightAlphabet
                        );

                        RunFSM.displayStatesColored(
                                bottomScrollPane,
                                graphViz,
                                draw,
                                Set.of(intersectionState),
                                bottomAlphabet
                        );
                        break;

                    case 3:
                        var intersectionTransition = (Triple<String, Character, String>)iterator.next();
                        var src = RunFSM.pairFromState(intersectionTransition.getFirst());
                        var dest = RunFSM.pairFromState(intersectionTransition.getThird());

                        draw.addTransition(
                                intersectionTransition.getFirst(),
                                intersectionTransition.getSecond(),
                                intersectionTransition.getThird()
                        );

                        descriptionText.setText(descriptionText.getText()
                                .concat("Added transition from: ")
                                .concat(intersectionTransition.getFirst())
                                .concat(" on \'")
                                .concat(intersectionTransition.getSecond().toString())
                                .concat("\' to: ")
                                .concat(intersectionTransition.getThird())
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);

                        RunFSM.displayTransitionsColored(
                                leftScrollPane,
                                graphViz,
                                firstMachine,
                                Set.of(new Triple<>(src.getFirst(), intersectionTransition.getSecond(), dest.getFirst())),
                                leftAlphabet
                        );

                        RunFSM.displayTransitionsColored(
                                rightScrollPane,
                                graphViz,
                                secondMachine,
                                Set.of(new Triple<>(src.getSecond(), intersectionTransition.getSecond(), dest.getSecond())),
                                rightAlphabet
                        );

                        RunFSM.displayTransitionsColored(
                                bottomScrollPane,
                                graphViz,
                                draw,
                                Set.of(intersectionTransition),
                                bottomAlphabet
                        );
                        break;

                    case 4:
                        var intersectionInitial = (String)iterator.next();
                        draw.setInitial(intersectionInitial);

                        var pairState = RunFSM.pairFromState(intersectionInitial);

                        descriptionText.setText(descriptionText.getText()
                                .concat("Set initial state to: ")
                                .concat(draw.getInitial())
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);

                        RunFSM.displayStatesColored(
                                leftScrollPane,
                                graphViz,
                                firstMachine,
                                Set.of(pairState.getFirst()),
                                leftAlphabet
                        );

                        RunFSM.displayStatesColored(
                                rightScrollPane,
                                graphViz,
                                secondMachine,
                                Set.of(pairState.getSecond()),
                                rightAlphabet
                        );

                        RunFSM.displayStatesColored(
                                bottomScrollPane,
                                graphViz,
                                draw,
                                Set.of(draw.getInitial()),
                                bottomAlphabet
                        );
                        break;

                    case 5:
                        var intersectionFinal = (String)iterator.next();
                        draw.addStateFinal(intersectionFinal);

                        var pairFinalState = RunFSM.pairFromState(intersectionFinal);

                        descriptionText.setText(descriptionText.getText()
                                .concat("Marked state ")
                                .concat(intersectionFinal)
                                .concat(" as final")
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);

                        RunFSM.displayStatesColored(
                                leftScrollPane,
                                graphViz,
                                firstMachine,
                                Set.of(pairFinalState.getFirst()),
                                leftAlphabet
                        );

                        RunFSM.displayStatesColored(
                                rightScrollPane,
                                graphViz,
                                secondMachine,
                                Set.of(pairFinalState.getSecond()),
                                rightAlphabet
                        );

                        RunFSM.displayStatesColored(
                                bottomScrollPane,
                                graphViz,
                                draw,
                                Set.of(intersectionFinal),
                                bottomAlphabet
                        );
                        break;
                }
                break;


            case Union:
                switch (step) {
                    case 0:
                        iterator.next();
                        break;

                    case 1:
                        var symbol = (Character)iterator.next();
                        draw.setAlphabet(Sets.union(draw.getAlphabet(), Set.of(symbol)));

                        descriptionText.setText(descriptionText.getText()
                                .concat("Added symbol \'")
                                .concat(symbol.toString())
                                .concat("\' to alphabet")
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);

                        BuildFSM.displayDiagramAndAlphabet(
                                bottomScrollPane,
                                graphViz,
                                draw,
                                bottomAlphabet
                        );
                        break;

                    case 2:
                        var stateFirst = (String)iterator.next();
                        draw.addState(stateFirst);

                        descriptionText.setText(descriptionText.getText()
                                .concat("Added state ")
                                .concat(stateFirst)
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);

                        RunFSM.displayStatesColored(
                                leftScrollPane,
                                graphViz,
                                firstMachine,
                                Set.of(stateFirst),
                                leftAlphabet
                        );

                        RunFSM.displayStatesColored(
                                bottomScrollPane,
                                graphViz,
                                draw,
                                Set.of(stateFirst),
                                bottomAlphabet
                        );
                        break;

                    case 3:
                        var stateSecond = (String)iterator.next();
                        draw.addState(stateSecond);

                        descriptionText.setText(descriptionText.getText()
                                .concat("Added state ")
                                .concat(stateSecond)
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);

                        RunFSM.displayStatesColored(
                                rightScrollPane,
                                graphViz,
                                firstMachine,
                                Set.of(stateSecond),
                                rightAlphabet
                        );

                        RunFSM.displayStatesColored(
                                bottomScrollPane,
                                graphViz,
                                draw,
                                Set.of(stateSecond),
                                bottomAlphabet
                        );
                        break;

                    case 4:
                        var stateNew = (String)iterator.next();
                        draw.addState(stateNew);

                        descriptionText.setText(descriptionText.getText()
                                .concat("Added state ")
                                .concat(stateNew)
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);

                        RunFSM.displayStatesColored(
                                bottomScrollPane,
                                graphViz,
                                draw,
                                Set.of(stateNew),
                                bottomAlphabet
                        );
                        break;

                    case 5:
                        var transitionFirst = (Triple<String, Character, String>)iterator.next();
                        var srcFirst = transitionFirst.getFirst();
                        var destFirst = transitionFirst.getThird();

                        draw.addTransition(
                                srcFirst,
                                transitionFirst.getSecond(),
                                destFirst
                        );

                        descriptionText.setText(descriptionText.getText()
                                .concat("Added transition from: ")
                                .concat(srcFirst)
                                .concat(" on \'")
                                .concat(transitionFirst.getSecond().toString())
                                .concat("\' to: ")
                                .concat(destFirst)
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);

                        RunFSM.displayTransitionsColored(
                                leftScrollPane,
                                graphViz,
                                firstMachine,
                                Set.of(new Triple<>(srcFirst, transitionFirst.getSecond(), destFirst)),
                                leftAlphabet
                        );

                        RunFSM.displayTransitionsColored(
                                bottomScrollPane,
                                graphViz,
                                draw,
                                Set.of(transitionFirst),
                                bottomAlphabet
                        );
                        break;

                    case 6:
                        var transitionSecond = (Triple<String, Character, String>)iterator.next();
                        var srcSecond = transitionSecond.getFirst();
                        var destSecond = transitionSecond.getThird();

                        draw.addTransition(
                                srcSecond,
                                transitionSecond.getSecond(),
                                destSecond
                        );

                        descriptionText.setText(descriptionText.getText()
                                .concat("Added transition from: ")
                                .concat(srcSecond)
                                .concat(" on \'")
                                .concat(transitionSecond.getSecond().toString())
                                .concat("\' to: ")
                                .concat(destSecond)
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);

                        RunFSM.displayTransitionsColored(
                                rightScrollPane,
                                graphViz,
                                secondMachine,
                                Set.of(new Triple<>(srcSecond, transitionSecond.getSecond(), destSecond)),
                                rightAlphabet
                        );

                        RunFSM.displayTransitionsColored(
                                bottomScrollPane,
                                graphViz,
                                draw,
                                Set.of(transitionSecond),
                                bottomAlphabet
                        );
                        break;

                    case 7:
                        var transitionNew = (Triple<String, Character, String>)iterator.next();
                        var srcNew = transitionNew.getFirst();
                        var destNew = transitionNew.getThird();

                        draw.addTransition(
                                srcNew,
                                transitionNew.getSecond(),
                                destNew
                        );

                        descriptionText.setText(descriptionText.getText()
                                .concat("Added transition from: ")
                                .concat(srcNew)
                                .concat(" on \'")
                                .concat(transitionNew.getSecond().toString())
                                .concat("\' to: ")
                                .concat(destNew)
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);

                        if (firstMachine.getInitial().equals(destNew))
                            RunFSM.displayStatesColored(
                                    leftScrollPane,
                                    graphViz,
                                    firstMachine,
                                    Set.of(destNew),
                                    leftAlphabet
                            );
                        else
                            RunFSM.displayStatesColored(
                                    rightScrollPane,
                                    graphViz,
                                    secondMachine,
                                    Set.of(destNew),
                                    rightAlphabet
                            );

                        RunFSM.displayTransitionsColored(
                                bottomScrollPane,
                                graphViz,
                                draw,
                                Set.of(transitionNew),
                                bottomAlphabet
                        );
                        break;

                    case 8:
                        var unionInitial = (String)iterator.next();
                        draw.setInitial(unionInitial);

                        descriptionText.setText(descriptionText.getText()
                                .concat("Set initial state to: ")
                                .concat(draw.getInitial())
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);

                        RunFSM.displayStatesColored(
                                bottomScrollPane,
                                graphViz,
                                draw,
                                Set.of(unionInitial),
                                bottomAlphabet
                        );
                        break;

                    case 9:
                        var unionFinalFirst = (String)iterator.next();
                        draw.addStateFinal(unionFinalFirst);

                        descriptionText.setText(descriptionText.getText()
                                .concat("Marked state ")
                                .concat(unionFinalFirst)
                                .concat(" as final")
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);

                        RunFSM.displayStatesColored(
                                leftScrollPane,
                                graphViz,
                                firstMachine,
                                Set.of(unionFinalFirst),
                                leftAlphabet
                        );

                        RunFSM.displayStatesColored(
                                bottomScrollPane,
                                graphViz,
                                draw,
                                Set.of(unionFinalFirst),
                                bottomAlphabet
                        );
                        break;

                    case 10:
                        var unionFinalSecond = (String)iterator.next();
                        draw.addStateFinal(unionFinalSecond);

                        descriptionText.setText(descriptionText.getText()
                                .concat("Marked state ")
                                .concat(unionFinalSecond)
                                .concat(" as final")
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);

                        RunFSM.displayStatesColored(
                                rightScrollPane,
                                graphViz,
                                secondMachine,
                                Set.of(unionFinalSecond),
                                rightAlphabet
                        );

                        RunFSM.displayStatesColored(
                                bottomScrollPane,
                                graphViz,
                                draw,
                                Set.of(unionFinalSecond),
                                bottomAlphabet
                        );
                        break;
                }
                break;


            case Concatenation:
                switch (step) {
                    case 0:
                        iterator.next();
                        firstMachine = new NormalFormFactory().get(NormalFormType.Piggy).apply(firstMachine);
                        secondMachine = new NormalFormFactory().get(NormalFormType.Piggy).apply(secondMachine);
                        BuildFSM.displayDiagramAndAlphabet(leftScrollPane, graphViz, firstMachine, leftAlphabet);
                        BuildFSM.displayDiagramAndAlphabet(rightScrollPane, graphViz, secondMachine, rightAlphabet);
                        descriptionText.setText(descriptionText.getText().concat("Constructed first machine to its piggy normal form").concat("\n"));
                        descriptionText.setText(descriptionText.getText().concat("Constructed second machine to its piggy normal form").concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);
                        break;

                    case 1:
                        var symbol = (Character)iterator.next();
                        draw.setAlphabet(Sets.union(draw.getAlphabet(), Set.of(symbol)));

                        descriptionText.setText(descriptionText.getText()
                                .concat("Added symbol \'")
                                .concat(symbol.toString())
                                .concat("\' to alphabet")
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);

                        BuildFSM.displayDiagramAndAlphabet(
                                bottomScrollPane,
                                graphViz,
                                draw,
                                bottomAlphabet
                        );
                        break;

                    case 2:
                        var stateFirst = (String)iterator.next();
                        draw.addState(stateFirst);

                        descriptionText.setText(descriptionText.getText()
                                .concat("Added state ")
                                .concat(stateFirst)
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);

                        RunFSM.displayStatesColored(
                                leftScrollPane,
                                graphViz,
                                firstMachine,
                                Set.of(stateFirst),
                                leftAlphabet
                        );

                        RunFSM.displayStatesColored(
                                bottomScrollPane,
                                graphViz,
                                draw,
                                Set.of(stateFirst),
                                bottomAlphabet
                        );
                        break;

                    case 3:
                        var stateSecond = (String)iterator.next();
                        draw.addState(stateSecond);

                        descriptionText.setText(descriptionText.getText()
                                .concat("Added state ")
                                .concat(stateSecond)
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);

                        RunFSM.displayStatesColored(
                                rightScrollPane,
                                graphViz,
                                firstMachine,
                                Set.of(stateSecond),
                                rightAlphabet
                        );

                        RunFSM.displayStatesColored(
                                bottomScrollPane,
                                graphViz,
                                draw,
                                Set.of(stateSecond),
                                bottomAlphabet
                        );
                        break;

                    case 4:
                        var transitionFirst = (Triple<String, Character, String>)iterator.next();
                        var srcFirst = transitionFirst.getFirst();
                        var destFirst = transitionFirst.getThird();

                        draw.addTransition(
                                srcFirst,
                                transitionFirst.getSecond(),
                                destFirst
                        );

                        descriptionText.setText(descriptionText.getText()
                                .concat("Added transition from: ")
                                .concat(srcFirst)
                                .concat(" on \'")
                                .concat(transitionFirst.getSecond().toString())
                                .concat("\' to: ")
                                .concat(destFirst)
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);

                        RunFSM.displayTransitionsColored(
                                leftScrollPane,
                                graphViz,
                                firstMachine,
                                Set.of(new Triple<>(srcFirst, transitionFirst.getSecond(), destFirst)),
                                leftAlphabet
                        );

                        RunFSM.displayTransitionsColored(
                                bottomScrollPane,
                                graphViz,
                                draw,
                                Set.of(transitionFirst),
                                bottomAlphabet
                        );
                        break;

                    case 5:
                        var transitionSecond = (Triple<String, Character, String>)iterator.next();
                        var srcSecond = transitionSecond.getFirst();
                        var destSecond = transitionSecond.getThird();

                        draw.addTransition(
                                srcSecond,
                                transitionSecond.getSecond(),
                                destSecond
                        );

                        descriptionText.setText(descriptionText.getText()
                                .concat("Added transition from: ")
                                .concat(srcSecond)
                                .concat(" on \'")
                                .concat(transitionSecond.getSecond().toString())
                                .concat("\' to: ")
                                .concat(destSecond)
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);

                        RunFSM.displayTransitionsColored(
                                rightScrollPane,
                                graphViz,
                                secondMachine,
                                Set.of(new Triple<>(srcSecond, transitionSecond.getSecond(), destSecond)),
                                rightAlphabet
                        );

                        RunFSM.displayTransitionsColored(
                                bottomScrollPane,
                                graphViz,
                                draw,
                                Set.of(transitionSecond),
                                bottomAlphabet
                        );
                        break;

                    case 6:
                        var transitionNew = (Triple<String, Character, String>)iterator.next();
                        var srcNew = transitionNew.getFirst();
                        var destNew = transitionNew.getThird();

                        draw.addTransition(
                                srcNew,
                                transitionNew.getSecond(),
                                destNew
                        );

                        descriptionText.setText(descriptionText.getText()
                                .concat("Added transition from: ")
                                .concat(srcNew)
                                .concat(" on \'")
                                .concat(transitionNew.getSecond().toString())
                                .concat("\' to: ")
                                .concat(destNew)
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);

                        RunFSM.displayStatesColored(
                                leftScrollPane,
                                graphViz,
                                firstMachine,
                                Set.of(srcNew),
                                leftAlphabet
                        );

                        RunFSM.displayStatesColored(
                                rightScrollPane,
                                graphViz,
                                secondMachine,
                                Set.of(destNew),
                                rightAlphabet
                        );

                        RunFSM.displayTransitionsColored(
                                bottomScrollPane,
                                graphViz,
                                draw,
                                Set.of(transitionNew),
                                bottomAlphabet
                        );
                        break;

                    case 8:
                        var concatInitial = (String)iterator.next();
                        draw.setInitial(concatInitial);

                        descriptionText.setText(descriptionText.getText()
                                .concat("Set initial state to: ")
                                .concat(draw.getInitial())
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);

                        RunFSM.displayStatesColored(
                                leftScrollPane,
                                graphViz,
                                firstMachine,
                                Set.of(concatInitial),
                                leftAlphabet
                        );

                        RunFSM.displayStatesColored(
                                bottomScrollPane,
                                graphViz,
                                draw,
                                Set.of(concatInitial),
                                bottomAlphabet
                        );
                        break;

                    case 9:
                        var concatFinal = (String)iterator.next();
                        draw.addStateFinal(concatFinal);

                        descriptionText.setText(descriptionText.getText()
                                .concat("Marked state ")
                                .concat(concatFinal)
                                .concat(" as final")
                                .concat("\n"));
                        descriptionScrollPane.setVvalue(1.0);

                        RunFSM.displayStatesColored(
                                rightScrollPane,
                                graphViz,
                                secondMachine,
                                Set.of(concatFinal),
                                rightAlphabet
                        );

                        RunFSM.displayStatesColored(
                                bottomScrollPane,
                                graphViz,
                                draw,
                                Set.of(concatFinal),
                                bottomAlphabet
                        );
                        break;
                }
                break;
        }
    }

    void runOperation(FSM first, FSM second, boolean direct) {
        firstMachine = first.copy();
        secondMachine = second.copy();
        this.direct = direct;
        operation = new BinaryOperationFactory().get(OpControllerMediator.getInstance().getType());
        steps = operation.getSteps();
        resultMachine = operation.apply(first, second);
        draw = new NFA("draw");
        algorithmLength = steps.size() - 1;
        step = 0;
        iterator = Set.of("convert").iterator();
        descriptionText.setText(steps.get(step).concat("\n"));
        descriptionScrollPane.setVvalue(1.0);
        BuildFSM.displayDiagramAndAlphabet(leftScrollPane, graphViz, first, leftAlphabet);
        BuildFSM.displayDiagramAndAlphabet(rightScrollPane, graphViz, second, rightAlphabet);
    }
}
